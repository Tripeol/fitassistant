package com.tripeo.fitassistant.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

/**
 * Кастомный {@link ViewPager} который не позволяет свайпать экраны
 *
 * @author Trikhin P O on 30.11.2018
 */
public class NonSwipableViewPager extends ViewPager {

    public NonSwipableViewPager(@NonNull Context context) {
        super(context);
    }

    public NonSwipableViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }
}
