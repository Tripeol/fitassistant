package com.tripeo.fitassistant.mvp.view.lists;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.commonrecyclerviewadapter.ListItemHolder;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;
import com.tripeo.fitassistant.utils.TimeUtils;

public class MainScreenListItemHolder extends ListItemHolder<MainScreenListItemBinder> {

    private static final int ANIMATION_DURATION = 1000;

    private ImageView mIconImageView;
    private TextView mTitleTextView;
    private TextView mMonthTotalTextView;
    private TextView mDayTotalTextView;
    private ViewGroup mRootLayout;

    private ValueAnimator mTodayTextViewAnimator;
    private int mTodayStartAnimatorValue;

    private ValueAnimator mMonthTextViewAnimator;
    private int mMonthStartAnimatorValue;

    public MainScreenListItemHolder(@NonNull View itemView) {
        super(itemView);
        mIconImageView = itemView.findViewById(R.id.image_view);
        mTitleTextView = itemView.findViewById(R.id.title_text_view);
        mMonthTotalTextView = itemView.findViewById(R.id.total_month_count_text_view);
        mDayTotalTextView = itemView.findViewById(R.id.total_day_count_text_view);
        mRootLayout = itemView.findViewById(R.id.root_layout);
        mTodayTextViewAnimator = new ValueAnimator();
        mMonthTextViewAnimator = new ValueAnimator();
    }

    @Override
    public void bind(MainScreenListItemBinder mainScreenListItemBinder) {
        mIconImageView.setImageResource(mainScreenListItemBinder.getImageRes());
        mTitleTextView.setText(mainScreenListItemBinder.getTitleTextRes());

        startCountAnimationForToday(
                mTodayTextViewAnimator,
                mTodayStartAnimatorValue,
                mainScreenListItemBinder.getDayCount(),
                valueAnimator -> {
                    long longValue = Long.valueOf(valueAnimator.getAnimatedValue().toString());
                    mDayTotalTextView.setText(
                            getDayCount(mDayTotalTextView.getContext(), makePrettyString(longValue, mainScreenListItemBinder.getDataType())));
                },
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mTodayStartAnimatorValue = mainScreenListItemBinder.getDayCount();
                    }
                });

        startCountAnimationForToday(
                mMonthTextViewAnimator,
                mMonthStartAnimatorValue,
                mainScreenListItemBinder.getTotalCount(),
                valueAnimator -> {
                    long longValue = Long.valueOf(valueAnimator.getAnimatedValue().toString());
                    mMonthTotalTextView.setText(
                            getTotalCount(mMonthTotalTextView.getContext(), makePrettyString(longValue, mainScreenListItemBinder.getDataType())));
                },
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mMonthStartAnimatorValue = mainScreenListItemBinder.getTotalCount();
                    }
                });

        mRootLayout.setOnClickListener(mainScreenListItemBinder.getOnClickListener());
    }

    private String getDayCount(Context context, String dayCount) {
        return String.format(context.getString(R.string.summary_adapter_today_pattern), dayCount);
    }

    private String getTotalCount(Context context, String totalCount) {
        return String.format(context.getString(R.string.summary_adapter_this_month_pattern), totalCount);
    }

    private void startCountAnimationForToday(
            ValueAnimator valueAnimator,
            int startValue,
            int endValue,
            ValueAnimator.AnimatorUpdateListener animatorUpdateListener,
            ValueAnimator.AnimatorListener animatorListener) {

        if (valueAnimator.isRunning()) {
            return;
        }

        valueAnimator = ValueAnimator.ofInt(startValue, endValue);
        valueAnimator.setDuration(ANIMATION_DURATION);
        valueAnimator.addUpdateListener(animatorUpdateListener);
        valueAnimator.addListener(animatorListener);

        valueAnimator.start();
    }

    private String makePrettyString(long initValue, @FitnessDataType int dataType) {
        switch (dataType) {
            case FitnessDataType.TIME:
                return TimeUtils.getTimeIntervalAsString(initValue);
            case FitnessDataType.DECIMAL:
            default:
                return String.valueOf(initValue);
        }
    }
}
