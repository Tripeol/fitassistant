package com.tripeo.fitassistant.mvp.viewmodel;

import androidx.lifecycle.ViewModel;

import com.tripeo.fitassistant.mvp.viewmodel.framework.SingleLiveEvent;

/**
 * ViewModel соответствующая основной {@link android.app.Activity} приложения
 *
 * @author Trikhin P O on 06.10.2018
 */
public class MainActivityViewModel extends ViewModel{

    private SingleLiveEvent<Void> mGoogleSignOutLiveData;

    public MainActivityViewModel() {
        mGoogleSignOutLiveData = new SingleLiveEvent<>();
    }

    /**
     * Получить {@link androidx.lifecycle.LiveData} испускающую данные об успехе сброса регистрации
     */
    public SingleLiveEvent<Void> getGoogleSignOutLiveData() {
        return mGoogleSignOutLiveData;
    }

    /**
     * Успешно выполнился запрос на сброс регистрации
     */
    public void onGoogleSignOut() {
        mGoogleSignOutLiveData.setValue(null);
    }
}
