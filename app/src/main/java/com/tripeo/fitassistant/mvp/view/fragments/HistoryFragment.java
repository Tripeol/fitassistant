package com.tripeo.fitassistant.mvp.view.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.commonrecyclerviewadapter.CommonAdapter;
import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.view.ExerciseActivity;
import com.tripeo.fitassistant.mvp.viewmodel.HistoryFragmentViewModel;

import java.util.List;

/**
 * Фрагмент содержащий в себе историю пользователя
 */
public class HistoryFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private CommonAdapter mAdapter;
    private HistoryFragmentViewModel mViewModel;

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    public HistoryFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mViewModel = ViewModelProviders.of(this).get(HistoryFragmentViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mViewModel.getExerciseListElementsLiveData().observe(this, abstractViewModels -> updateListData(abstractViewModels));
        mViewModel.getListElementClickLiveData().observe(this, exerciseType -> navigateToHistoryExerciseScreen(exerciseType));

        View fragmentView = inflater.inflate(R.layout.fragment_history, container, false);
        configureRecyclerView(fragmentView);

        return fragmentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewModel.getExerciseListElementsLiveData().removeObservers(this);
        mViewModel.getListElementClickLiveData().removeObservers(this);
    }

    private void updateListData(List<ListItemBinder> entityList) {
        mAdapter.setBinders(entityList);
    }

    private void navigateToHistoryExerciseScreen(ExerciseType exerciseType) {
        startActivity(ExerciseActivity.newIntent(getContext(), exerciseType));
    }

    private void configureRecyclerView(View fragmentView) {
        mRecyclerView = fragmentView.findViewById(R.id.recycler_view);
        mAdapter = new CommonAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

}
