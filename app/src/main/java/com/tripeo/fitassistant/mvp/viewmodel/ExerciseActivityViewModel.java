package com.tripeo.fitassistant.mvp.viewmodel;

import android.app.Application;
import android.text.format.DateUtils;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.google.android.gms.fitness.data.DataPoint;
import com.tripeo.fitassistant.FitAssistantApplication;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.managers.interfaces.IFitnessRepository;
import com.tripeo.fitassistant.managers.interfaces.IResourceManager;
import com.tripeo.fitassistant.mvp.model.ExerciseBarEntry;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;
import com.tripeo.fitassistant.mvp.view.ScreenState;
import com.tripeo.fitassistant.mvp.view.lists.ExerciseListItemBinder;
import com.tripeo.fitassistant.mvp.viewmodel.framework.SingleLiveEvent;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;
import com.tripeo.fitassistant.utils.ExerciseTypeUtils;
import com.tripeo.fitassistant.utils.FitnessUtils;
import com.tripeocorp.calendarutils.CalendarUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

/**
 * ViewModel соответствующая экрану с информацией о конкретном упражнении.
 *
 * @author Trikhin P O on 30.09.2018
 */
public class ExerciseActivityViewModel extends AndroidViewModel {

    private static final float BAR_WIDTH = 0.8f;

    @Inject
    IFitnessRepository mFitnessManager;
    @Inject
    IResourceManager mResourceManager;

    private ExerciseType mExerciseType;
    private AbstractExersizeTypeHelper mExersizeHelper;

    private List<DataPoint> mDataPoints;
    private Entry mSelectedEntry;

    // Позволяет установить заголовок экрана
    private MutableLiveData<String> mTitleLiveData;
    // Позволяет установить общее количество сделанных единиц данного упражнения
    private MutableLiveData<String> mTotalCountLiveData;
    // Позволяет установить состояние экрана (контент, загрузка, ошибка)
    private MutableLiveData<Integer> mScreenStateLiveData;
    // Позволяет установить данные для графика
    private MutableLiveData<BarData> mBarGraphLiveData;
    // Позволяет установить данные для списка подходов под графиком
    private MutableLiveData<List<ListItemBinder>> mExerciseListLiveData;
    // Позволяет установить дату выбранного дня
    private MutableLiveData<String> mSelectedDayLiveData;
    // Позволяет установить общее количество сделанных единиц упражнения в выбранный день
    private MutableLiveData<String> mSelectedDayCountLiveData;
    // Позволяет установить указатель на выбранный день на графике
    private MutableLiveData<Entry> mSelectedEntryLiveData;
    // Отвечает за события нажатия на элемент нижнего списка подходов под графиком
    private SingleLiveEvent<DataPoint> mListElementClickLiveData;



    public ExerciseActivityViewModel(@NonNull Application application, ExerciseType exerciseType) {
        super(application);
        mExerciseType = exerciseType;
        mExersizeHelper = ExerciseTypeUtils.getHelperByType(mExerciseType);

        mTotalCountLiveData = new MutableLiveData<>();
        mTitleLiveData = new MutableLiveData<>();
        mScreenStateLiveData = new MutableLiveData<>();
        mBarGraphLiveData = new MutableLiveData<>();
        mExerciseListLiveData = new MutableLiveData<>();
        mSelectedDayLiveData = new MutableLiveData<>();
        mSelectedDayCountLiveData = new MutableLiveData<>();
        mSelectedEntryLiveData = new MutableLiveData<>();
        mListElementClickLiveData = new SingleLiveEvent<>();

        ((FitAssistantApplication) application).getAppComponent().inject(this);

        mScreenStateLiveData.postValue(ScreenState.LOADING);
        readData();

        mTitleLiveData.postValue(getTitle());
    }

    public MutableLiveData<String> getTotalCountLiveData() {
        return mTotalCountLiveData;
    }

    public MutableLiveData<String> getTitleLiveData() {
        return mTitleLiveData;
    }

    public MutableLiveData<Integer> getScreenStateLiveData() {
        return mScreenStateLiveData;
    }

    public MutableLiveData<BarData> getBarGraphLiveData() {
        return mBarGraphLiveData;
    }

    public MutableLiveData<List<ListItemBinder>> getExerciseListLiveData() {
        return mExerciseListLiveData;
    }

    public MutableLiveData<String> getSelectedDayLiveData() {
        return mSelectedDayLiveData;
    }

    public MutableLiveData<String> getSelectedDayCountLiveData() {
        return mSelectedDayCountLiveData;
    }

    public MutableLiveData<Entry> getSelectedEntryLiveData() {
        return mSelectedEntryLiveData;
    }

    public SingleLiveEvent<DataPoint> getListElementClickLiveData() {
        return mListElementClickLiveData;
    }

    /**
     * Пользователь выбрал какое-то значение на графике.
     */
    public void onValueSelected(Entry e) {
        mSelectedEntry = e;
        mSelectedEntryLiveData.postValue(e);
        drawViewsDependOnSelectedEntry(((ExerciseBarEntry) e).getDataPoints(), ((ExerciseBarEntry) e).getEntryDate());
    }

    /**
     * Пользователь удалил один из подходов {@link DataPoint}
     */
    public void onDataPointRemoved(DataPoint dataPoint) {
        mDataPoints.remove(dataPoint);
        drawGraph(mDataPoints);
        drawTotalCount(mDataPoints);

        List<DataPoint> selectedDataPoints = ((ExerciseBarEntry) mSelectedEntry).getDataPoints();
        selectedDataPoints.remove(dataPoint);
        drawViewsDependOnSelectedEntry(selectedDataPoints, ((ExerciseBarEntry) mSelectedEntry).getEntryDate());
    }

    private void readData() {
        Date from = new Date(1);
        Date to = Calendar.getInstance().getTime();
        mFitnessManager.readData(new ArrayList<ExerciseType>() {{
                                     add(mExerciseType);
                                 }},
                from,
                to,
                dataReadResponse -> {
                    mScreenStateLiveData.postValue(ScreenState.CONTENT);
                    List<DataPoint> dataPoints = FitnessUtils.getDataPointsByType(dataReadResponse, mExerciseType);
                    mDataPoints = dataPoints;
                    drawGraph(dataPoints);
                    drawTotalCount(dataPoints);
                }, e -> {
                    mScreenStateLiveData.postValue(ScreenState.ERROR);
                });
    }

    private void drawGraph(List<DataPoint> dataPoints) {
        @FitnessDataType
        int dataType = ExerciseTypeUtils.getHelperByType(mExerciseType).getFitnessDataType();

        BarDataSet set = null;
        set = new BarDataSet(constructBarEntities(dataPoints, dataType), "BarDataSet");
        set.setDrawValues(false);
        set.setColor(mResourceManager.getColorFromResource(R.color.colorPrimary));
        BarData data = new BarData(set);
        data.setBarWidth(BAR_WIDTH);
        mBarGraphLiveData.setValue(data);
    }

    private void drawTotalCount(List<DataPoint> dataPoints) {
        String resultValue = mExersizeHelper.getCalculator().getTotalValueAsPrettyString(dataPoints);
        mTotalCountLiveData.postValue(constructTotalCountString(resultValue));
    }

    private void drawViewsDependOnSelectedEntry(List<DataPoint> dataPoints, long date ) {
        mExerciseListLiveData.postValue(convertDataPointsToListItems(dataPoints));
        mSelectedDayLiveData.postValue(convertDateToString(date));
        if (!dataPoints.isEmpty()) {
            String stringValue = mExersizeHelper.getCalculator().getTotalValueAsPrettyString(dataPoints);
            mSelectedDayCountLiveData.postValue(constructDayTotalCountString(stringValue));
        } else {
            mSelectedDayCountLiveData.postValue(mResourceManager.getStringFromResource(R.string.empty_history_day));
        }
    }

    private List<BarEntry> constructBarEntities(List<DataPoint> dataPoints, @FitnessDataType int dataType) {
        LongSparseArray<List<DataPoint>> mergedMap = new LongSparseArray<>();
        for (DataPoint dataPoint : dataPoints) {
            long day = dataPoint.getEndTime(TimeUnit.DAYS);
            if (mergedMap.get(day) == null) {
                mergedMap.put(day, new ArrayList() {{
                    add(dataPoint);
                }});
            } else {
                List<DataPoint> existList = mergedMap.get(day);
                existList.add(dataPoint);
                mergedMap.put(day, existList);
            }
        }

        List<BarEntry> result = new ArrayList<>();

        //К текущему моменту в mergedMap в порядке возрастания находятся списки упражнений, а ключ - номер дня
        long timeInDayInterval;
        for (int i = 0; i < mergedMap.size(); i++) {
            //Создаем бары соответствующие дням в которых есть подходы
            timeInDayInterval = mergedMap.valueAt(i).get(0).getEndTime(TimeUnit.MILLISECONDS);
            result.add(new ExerciseBarEntry(
                    result.size(),
                    mExersizeHelper.getCalculator().getTotalValue(mergedMap.valueAt(i)),
                    mergedMap.valueAt(i),
                    timeInDayInterval));

            //Создаем бары соответствующие дням в которых нет подоходов
            if (i < mergedMap.size() - 1) {
                long numberOfDaysToFill = mergedMap.keyAt(i + 1) - mergedMap.keyAt(i) - 1;
                for (long j = 0; j < numberOfDaysToFill; j++) {
                    timeInDayInterval = timeInDayInterval + DateUtils.DAY_IN_MILLIS;
                    result.add(
                            new ExerciseBarEntry(result.size(),
                                    0,
                                    new ArrayList<>(),
                                    timeInDayInterval));
                }
            }
        }

        // к этому моменту в result находятся все нужные значения, остается только заполнить пустыми полями до текущего дня
        if (!result.isEmpty()) {
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTimeInMillis(((ExerciseBarEntry) result.get(result.size() - 1)).getEntryDate());
            long difInDays = CalendarUtils.getDiffInDays(startCalendar, Calendar.getInstance());

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(startCalendar.getTimeInMillis());
            for (int i = 0; i < difInDays; i++) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                result.add(new ExerciseBarEntry(result.size(),
                        0,
                        new ArrayList<>(),
                        calendar.getTimeInMillis()));
            }
        }

        return result;
    }

    private String getTitle() {
        return mResourceManager.getStringFromResource(ExerciseTypeUtils.getHelperByType(mExerciseType).getName());
    }

    private List<ListItemBinder> convertDataPointsToListItems(List<DataPoint> dataPoints) {
        List<ListItemBinder> result = new ArrayList<>();
        for (int i = 0; i < dataPoints.size(); i++) {
            int finalI = i;
            result.add(new ExerciseListItemBinder(dataPoints.get(i),
                    mExerciseType,
                    i == dataPoints.size() - 1,
                    view -> {
                        mListElementClickLiveData.postValue(dataPoints.get(finalI));
                    }));
        }
        return result;
    }

    private String convertDateToString(long dateInMillis) {
        Date date = new Date(dateInMillis);
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.forLanguageTag("RUS"));
        return dateFormat.format(date);
    }

    private String constructTotalCountString(String totalValue) {
        String pattern = mResourceManager.getStringFromResource(R.string.integer_total_count_pattern);
        return String.format(pattern, totalValue);
    }

    private String constructDayTotalCountString(String totalValue) {
        String pattern = mResourceManager.getStringFromResource(R.string.selected_day_count_pattern);
        return String.format(pattern, totalValue);
    }
}
