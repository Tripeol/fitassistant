package com.tripeo.fitassistant.mvp.viewmodel;

import androidx.lifecycle.ViewModel;

import com.tripeo.fitassistant.mvp.viewmodel.framework.SingleLiveEvent;

/**
 * {@link androidx.lifecycle.ViewModel} соответствующая экрану приветствия
 *
 * @author Trikhin P O on 06.10.2018
 */
public class SplashActivityViewModel extends ViewModel {

    private SingleLiveEvent<Void> mCheckGoogleFitAccessLiveData;
    private SingleLiveEvent<Void> mCheckPermissionsLiveData;
    private SingleLiveEvent<Void> mNavigateToMainMenuLiveData;
    private SingleLiveEvent<Void> mShowGoogleFitExplanationDialog;
    private SingleLiveEvent<Void> mShowPermissionExplanationDialog;

    public SplashActivityViewModel() {
        mCheckGoogleFitAccessLiveData = new SingleLiveEvent<>();
        mCheckPermissionsLiveData = new SingleLiveEvent<>();
        mNavigateToMainMenuLiveData = new SingleLiveEvent<>();
        mShowGoogleFitExplanationDialog = new SingleLiveEvent<>();
        mShowPermissionExplanationDialog = new SingleLiveEvent<>();
    }

    public SingleLiveEvent<Void> getCheckGoogleFitAccessLiveData() {
        return mCheckGoogleFitAccessLiveData;
    }

    public SingleLiveEvent<Void> getCheckPermissionsLiveData() {
        return mCheckPermissionsLiveData;
    }

    public SingleLiveEvent<Void> getNavigateToMainMenuLiveData() {
        return mNavigateToMainMenuLiveData;
    }

    public SingleLiveEvent<Void> getShowGoogleFitExplanationDialog() {
        return mShowGoogleFitExplanationDialog;
    }

    public SingleLiveEvent<Void> getShowPermissionExplanationDialog() {
        return mShowPermissionExplanationDialog;
    }

    /**
     * пытается получить доступ к GoogleFit
     */
    public void tryToGetGoogleFitAccess() {
        mCheckGoogleFitAccessLiveData.setValue(null);
    }

    /**
     * Пользователь выдал права на GoogleFit
     */
    public void onGoogleFitAccessCheckedAndGranted() {
        mCheckPermissionsLiveData.setValue(null );
    }

    /**
     * Пользователь выдал разрешения для работы со звуком
     */
    public void onPermissionsGranted(){
        mNavigateToMainMenuLiveData.setValue(null);
    }

    /**
     * Пользователь не выдал разрешения для работы со звуком
     */
    public void onPermissionsDenied() {
        mShowPermissionExplanationDialog.setValue(null);
    }

    /**
     * Пользователь не выдал права на гугл фит
     */
    public void onGoogleFitAccessDenied() {
        mShowGoogleFitExplanationDialog.setValue(null);
    }

}
