package com.tripeo.fitassistant.mvp.view.lists;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.commonrecyclerviewadapter.ListItemHolder;
import com.tripeo.fitassistant.R;

/**
 * {@link SettingsListItemHolder} Соответствующий элементу списка на экране настроек
 *
 * @author Trikhin P O
 */
public class SettingsListItemHolder extends ListItemHolder<SettingsListItemBinder> {

    private ImageView mIconImageView;
    private TextView mTextView;

    public SettingsListItemHolder(@NonNull View itemView) {
        super(itemView);
        mIconImageView = itemView.findViewById(R.id.icon_image_view);
        mTextView = itemView.findViewById(R.id.text_view);
    }

    @Override
    public void bind(SettingsListItemBinder settingsListItemBinder) {
        mIconImageView.setImageResource(settingsListItemBinder.getIconRes());
        mTextView.setText(settingsListItemBinder.getTextRes());
        mTextView.setOnClickListener(settingsListItemBinder.getOnClickListener());
    }
}
