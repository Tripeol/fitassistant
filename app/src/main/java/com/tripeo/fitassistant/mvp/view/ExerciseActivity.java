package com.tripeo.fitassistant.mvp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.commonrecyclerviewadapter.CommonAdapter;
import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.google.android.gms.fitness.data.DataPoint;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.view.fragments.OnDataPointRemovedListener;
import com.tripeo.fitassistant.mvp.view.fragments.dialogs.DataPointActionsBottomSheetDialog;
import com.tripeo.fitassistant.mvp.viewmodel.ExerciseActivityViewModel;
import com.tripeo.fitassistant.mvp.viewmodel.factories.ExerciseActivityViewModelFactory;

import java.util.List;

/**
 * {@link android.app.Activity} соответствующая экрану с историей конкретного упражнения
 *
 * @author Trikhin P O on 27.09.2018
 */
public class ExerciseActivity extends AppCompatActivity implements OnDataPointRemovedListener {

    private static final String EXERCISE_TYPE_KEY = "exercise_type";
    private static final int NUMBER_OF_BARS_ON_SCREEN = 20;

    private ExerciseType mExerciseType;
    private ExerciseActivityViewModel mExerciseActivityViewModel;
    private Entry mSelectedEntry;

    private TextView mTitleTextView;
    private TextView mTotalCountTextView;
    private TextView mSelectedDayDateTextView;
    private TextView mSelectedDayCountTextView;
    private BarChart mBarChart;
    private RecyclerView mRecyclerView;
    private CommonAdapter mAdapter;
    private ViewGroup mContentLayout;
    private ViewGroup mErrorLayout;
    private ProgressBar mProgressLayout;

    public static Intent newIntent(@NonNull Context context, @NonNull ExerciseType exerciseType) {
        Intent intent = new Intent(context, ExerciseActivity.class);
        intent.putExtra(EXERCISE_TYPE_KEY, exerciseType);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        if (savedInstanceState == null) {
            decodeBundle(getIntent().getExtras());
        } else {
            decodeBundle(savedInstanceState);
        }

        setupViews();

        mExerciseActivityViewModel = ViewModelProviders.of(this, new ExerciseActivityViewModelFactory(getApplication(), mExerciseType))
                .get(ExerciseActivityViewModel.class);
        mExerciseActivityViewModel.getTotalCountLiveData().observe(this, s -> mTotalCountTextView.setText(s));
        mExerciseActivityViewModel.getTitleLiveData().observe(this, title -> mTitleTextView.setText(title));
        mExerciseActivityViewModel.getScreenStateLiveData().observe(this, screenState -> showScreenState(screenState));
        mExerciseActivityViewModel.getBarGraphLiveData().observe(this, barData -> loadGraphWithData(barData));
        mExerciseActivityViewModel.getExerciseListLiveData().observe(this, listItems -> updateExerciseListData(listItems));
        mExerciseActivityViewModel.getSelectedDayLiveData().observe(this, dateAsStr -> updateSelectedDayTitle(dateAsStr));
        mExerciseActivityViewModel.getSelectedDayCountLiveData().observe(this, count -> updateSelectedDayCount(count));
        mExerciseActivityViewModel.getSelectedEntryLiveData().observe(this, entry -> updateSelectedEntry(entry));
        mExerciseActivityViewModel.getListElementClickLiveData().observe(this, dataPoint -> showBottomDialog(dataPoint));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXERCISE_TYPE_KEY, mExerciseType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDataPointRemoved(DataPoint dataPoint) {
        mExerciseActivityViewModel.onDataPointRemoved(dataPoint);
    }

    private void updateSelectedDayTitle(String text) {
        mSelectedDayDateTextView.setText(text);
    }

    private void updateSelectedDayCount(String text) {
        mSelectedDayCountTextView.setText(text);
    }

    private void updateExerciseListData(List<ListItemBinder> binders) {
        mAdapter.setBinders(binders);
        mAdapter.notifyDataSetChanged();
    }

    private void updateSelectedEntry(Entry e) {
        if (mSelectedEntry != null) {
            mSelectedEntry.setIcon(null);
        }
        mSelectedEntry = e;
        mSelectedEntry.setIcon(getDrawable(R.drawable.ic_place_accent));
        mBarChart.invalidate();
    }

    private void loadGraphWithData(BarData barData) {
        mBarChart.setData(barData);
        mBarChart.invalidate();
        //Незнаю почему но то что написано ниже обязательно должно быть после инвалидейта
        mBarChart.setVisibleXRange(NUMBER_OF_BARS_ON_SCREEN, NUMBER_OF_BARS_ON_SCREEN);
        if (barData.getDataSetCount() > 0) {
            int numberOfBars = barData.getDataSets().get(0).getEntryCount();
            mBarChart.moveViewToX(numberOfBars - NUMBER_OF_BARS_ON_SCREEN);
        }
    }

    private void decodeBundle(@NonNull Bundle bundle) {
        if (bundle.containsKey(EXERCISE_TYPE_KEY)) {
            mExerciseType = (ExerciseType) bundle.getSerializable(EXERCISE_TYPE_KEY);
        }
    }

    private void showBottomDialog(DataPoint dataPoint) {
        DataPointActionsBottomSheetDialog bottomSheetDialog = DataPointActionsBottomSheetDialog.newInstance(dataPoint);
        bottomSheetDialog.show(getSupportFragmentManager(), DataPointActionsBottomSheetDialog.TAG);
    }

    private void setupViews() {
        mTotalCountTextView = findViewById(R.id.total_text_view);
        mTitleTextView = findViewById(R.id.title_text_view);
        mProgressLayout = findViewById(R.id.progress_bar);
        mErrorLayout = findViewById(R.id.error_layout);
        mContentLayout = findViewById(R.id.content_layout);
        mBarChart = findViewById(R.id.bar_chart_view);
        mSelectedDayDateTextView = findViewById(R.id.current_day_text_view);
        mSelectedDayCountTextView = findViewById(R.id.day_count_text_view);

        mRecyclerView = findViewById(R.id.exercise_recycler_view);
        mAdapter = new CommonAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mBarChart.setFitBars(true);
        mBarChart.setScaleEnabled(false);
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.getAxisRight().setDrawAxisLine(false);
        mBarChart.getAxisLeft().setEnabled(false);
        mBarChart.getLegend().setEnabled(false);
        mBarChart.getDescription().setEnabled(false);
        mBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        mBarChart.getXAxis().setDrawGridLines(false);
        mBarChart.getXAxis().setDrawLabels(false);
        mBarChart.getAxisLeft().setAxisMinimum(0);
        mBarChart.getAxisRight().setAxisMinimum(0);
        mBarChart.setOnChartGestureListener(new ChartGestureListener());
        mBarChart.setHighlightFullBarEnabled(true);
        mBarChart.invalidate();
    }

    private void showScreenState(@ScreenState int screenState) {
        switch (screenState) {
            case ScreenState.CONTENT:
                mContentLayout.setVisibility(View.VISIBLE);
                mProgressLayout.setVisibility(View.GONE);
                mErrorLayout.setVisibility(View.GONE);
                break;
            case ScreenState.LOADING:
                mContentLayout.setVisibility(View.GONE);
                mProgressLayout.setVisibility(View.VISIBLE);
                mErrorLayout.setVisibility(View.GONE);
                break;
            case ScreenState.ERROR:
                mContentLayout.setVisibility(View.GONE);
                mProgressLayout.setVisibility(View.GONE);
                mErrorLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    private class ChartGestureListener implements OnChartGestureListener {

        @Override
        public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

        }

        @Override
        public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

        }

        @Override
        public void onChartLongPressed(MotionEvent me) {

        }

        @Override
        public void onChartDoubleTapped(MotionEvent me) {

        }

        @Override
        public void onChartSingleTapped(MotionEvent me) {
            Entry e = mBarChart.getEntryByTouchPoint(me.getX(), me.getY());
            if (e != null) {
                e.setIcon(getDrawable(R.drawable.ic_place_accent));
                mExerciseActivityViewModel.onValueSelected(e);
            }
        }

        @Override
        public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

        }

        @Override
        public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

        }

        @Override
        public void onChartTranslate(MotionEvent me, float dX, float dY) {

        }
    }
}
