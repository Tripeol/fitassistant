package com.tripeo.fitassistant.mvp.view.lists;

import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;

public class MainScreenListItemBinder extends ListItemBinder<MainScreenListItemHolder> {

    @DrawableRes
    private int mImageRes;
    @StringRes
    private int mTitleTextRes;
    private int mTotalCount;
    private int mDayCount;
    @FitnessDataType
    private int mDataType;
    private View.OnClickListener mOnClickListener;

    public MainScreenListItemBinder(@DrawableRes int image,
                                    @StringRes int title,
                                    int totalCount,
                                    int dayCount,
                                    @FitnessDataType int dataType,
                                    View.OnClickListener onClickListener) {
        super(R.layout.exersize_summary_list_item);
        mImageRes = image;
        mTitleTextRes = title;
        mTotalCount = totalCount;
        mDayCount = dayCount;
        mDataType = dataType;
        mOnClickListener = onClickListener;
    }

    @Override
    public MainScreenListItemHolder createCommonViewHolder(View view) {
        return new MainScreenListItemHolder(view);
    }

    public int getImageRes() {
        return mImageRes;
    }

    public int getTitleTextRes() {
        return mTitleTextRes;
    }

    public int getTotalCount() {
        return mTotalCount;
    }

    public int getDayCount() {
        return mDayCount;
    }

    @FitnessDataType
    public int getDataType() {
        return mDataType;
    }

    public View.OnClickListener getOnClickListener() {
        return mOnClickListener;
    }
}
