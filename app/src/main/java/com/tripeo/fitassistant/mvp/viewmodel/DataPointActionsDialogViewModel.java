package com.tripeo.fitassistant.mvp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.fitness.data.DataPoint;
import com.tripeo.fitassistant.FitAssistantApplication;
import com.tripeo.fitassistant.managers.interfaces.IFitnessRepository;
import com.tripeo.fitassistant.mvp.viewmodel.framework.SingleLiveEvent;

import javax.inject.Inject;

/**
 * {@link ViewModel} соответствующая диалогу {@link com.tripeo.fitassistant.mvp.view.fragments.dialogs.DataPointActionsBottomSheetDialog}
 */
public class DataPointActionsDialogViewModel extends AndroidViewModel {

    @Inject
    IFitnessRepository mIFitnessRepository;
    /**
     * {@link SingleLiveEvent} оповещающий о том что подход был успешно удален в GoogleFit
     */
    private SingleLiveEvent<Boolean> mOnDataPointRemovedLiveEvent;
    /**
     * {@link SingleLiveEvent} оповещающий о том что нажата кнопка редактировать
     */
    private SingleLiveEvent<Void> mOnEditRemoveLiveEvent;

    private DataPoint mDataPoint;

    public DataPointActionsDialogViewModel(@NonNull Application application, @NonNull DataPoint dataPoint) {
        super(application);

        mOnDataPointRemovedLiveEvent = new SingleLiveEvent<>();
        mOnEditRemoveLiveEvent = new SingleLiveEvent<>();

        mDataPoint = dataPoint;
        ((FitAssistantApplication) application).getAppComponent().inject(this);
    }

    public SingleLiveEvent<Boolean> getOnDataPointRemovedLiveEvent() {
        return mOnDataPointRemovedLiveEvent;
    }

    /**
     * Пользователь нажал на кнопку удаления подхода.
     */
    public void onDeleteButtonClicked() {
        mIFitnessRepository.deleteData(mDataPoint,
                aVoid -> mOnDataPointRemovedLiveEvent.postValue(true),
                e -> mOnDataPointRemovedLiveEvent.postValue(false));
    }

    /**
     * Пользователь нажал на кнопку "редактировать"
     */
    public void onEditButtonClicked() {

    }

}
