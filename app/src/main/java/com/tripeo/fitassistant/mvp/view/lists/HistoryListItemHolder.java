package com.tripeo.fitassistant.mvp.view.lists;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.commonrecyclerviewadapter.ListItemHolder;
import com.tripeo.fitassistant.R;

public class HistoryListItemHolder extends ListItemHolder<HistoryListItemBinder> {

    private ImageView mIconImageView;
    private TextView mTitleTextView;
    private ViewGroup mRoot;
    private View mDivider;


    public HistoryListItemHolder(@NonNull View itemView) {
        super(itemView);
        mIconImageView = itemView.findViewById(R.id.image_view);
        mTitleTextView = itemView.findViewById(R.id.title_text_view);
        mDivider = itemView.findViewById(R.id.divider_view);
        mRoot = itemView.findViewById(R.id.root_layout);
    }

    @Override
    public void bind(HistoryListItemBinder historyListItemBinder) {
        mIconImageView.setImageResource(historyListItemBinder.getImageRes());
        mTitleTextView.setText(historyListItemBinder.getTitleRes());
        mRoot.setOnClickListener(historyListItemBinder.getOnClickListener());
        if (historyListItemBinder.isLastItemInList()) {
            mDivider.setVisibility(View.GONE);
        } else {
            mDivider.setVisibility(View.VISIBLE);
        }
    }
}
