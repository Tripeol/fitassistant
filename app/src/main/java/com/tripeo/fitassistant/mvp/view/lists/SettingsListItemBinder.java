package com.tripeo.fitassistant.mvp.view.lists;

import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.R;

/**
 * {@link ListItemBinder} соответствующий элементу списка на экране настроек
 *
 * @author Trikhin P O
 */
public class SettingsListItemBinder extends ListItemBinder<SettingsListItemHolder> {

    @DrawableRes
    private final int mIconRes;
    @StringRes
    private final int mTextRes;
    private final View.OnClickListener mOnClickListener;

    /**
     * @param iconRes         ресурс иконки в отображаемом элементе
     * @param textRes         ресурс текста в отображаемом элементе
     * @param onClickListener обработчик нажатия на элемент
     */
    public SettingsListItemBinder(@DrawableRes int iconRes,
                                  @StringRes int textRes,
                                  View.OnClickListener onClickListener) {
        super(R.layout.settings_list_item);
        mIconRes = iconRes;
        mTextRes = textRes;
        mOnClickListener = onClickListener;
    }

    @Override
    public SettingsListItemHolder createCommonViewHolder(View view) {
        return new SettingsListItemHolder(view);
    }

    public int getIconRes() {
        return mIconRes;
    }

    public int getTextRes() {
        return mTextRes;
    }

    public View.OnClickListener getOnClickListener() {
        return mOnClickListener;
    }
}
