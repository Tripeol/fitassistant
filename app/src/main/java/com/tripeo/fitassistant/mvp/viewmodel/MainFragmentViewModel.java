package com.tripeo.fitassistant.mvp.viewmodel;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.tripeo.fitassistant.FitAssistantApplication;
import com.tripeo.fitassistant.managers.interfaces.IAIManager;
import com.tripeo.fitassistant.managers.interfaces.IAIParser;
import com.tripeo.fitassistant.managers.interfaces.IFitnessRepository;
import com.tripeo.fitassistant.managers.interfaces.ITTSManager;
import com.tripeo.fitassistant.managers.interfaces.IUserSettingsManager;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.view.ScreenState;
import com.tripeo.fitassistant.mvp.view.lists.MainScreenListItemBinder;
import com.tripeo.fitassistant.mvp.viewmodel.framework.SingleLiveEvent;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;
import com.tripeo.fitassistant.utils.DataPointUtils;
import com.tripeo.fitassistant.utils.ExerciseTypeUtils;
import com.tripeo.fitassistant.utils.FitnessUtils;
import com.tripeocorp.calendarutils.CalendarUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ai.api.AIListener;
import ai.api.model.AIError;
import ai.api.model.AIResponse;

/**
 * {@link ViewModel} соответствующая главному экрану приложения
 *
 * @author Trikhin P O
 * on 10.10.2018
 */
public class MainFragmentViewModel extends AndroidViewModel {

    private static final String TAG = MainFragmentViewModel.class.getSimpleName();

    @Inject
    IAIManager mAIManager;
    @Inject
    ITTSManager mTTSManager;
    @Inject
    IAIParser mAIParser;
    @Inject
    IFitnessRepository mFitnessManager;
    @Inject
    IUserSettingsManager mUserSettingsManager;

    private SingleLiveEvent<String> mToastMessageLiveData;
    private MutableLiveData<Integer> mScreenStateLiveData;
    private MutableLiveData<List<ListItemBinder>> mListDataLiveData;
    private MutableLiveData<Boolean> mStartSpeechAnimationLiveData;
    private SingleLiveEvent<ExerciseType> mListElementClickLiveData;
    //Оповещает подписчиков о том что пользователь нажал на кнопку с информацией
    private SingleLiveEvent<Void> mInformationButtonClickLiveData;

    public MainFragmentViewModel(@NonNull Application application) {
        super(application);
        ((FitAssistantApplication) application).getAppComponent().inject(this);

        mToastMessageLiveData = new SingleLiveEvent<>();
        mScreenStateLiveData = new MutableLiveData<>();
        mListDataLiveData = new MutableLiveData<>();
        mStartSpeechAnimationLiveData = new MutableLiveData<>();
        mListElementClickLiveData = new SingleLiveEvent<>();
        mInformationButtonClickLiveData = new SingleLiveEvent<>();

        mAIManager.setListener(new MainActivityAiListener());
        mScreenStateLiveData.setValue(ScreenState.LOADING);

        Date from = CalendarUtils.getBeginningOfMonthCalendar(Calendar.getInstance()).getTime();
        Date to = Calendar.getInstance().getTime();
        mFitnessManager.readData(
                Arrays.asList(ExerciseType.values()),
                from,
                to,
                new GoogleFitOnSuccessListener(),
                new GoogleFitOnErrorListener()
        );
    }


    public MutableLiveData<String> getToastMessageLiveData() {
        return mToastMessageLiveData;
    }

    public MutableLiveData<Integer> getScreenStateLiveData() {
        return mScreenStateLiveData;
    }

    public MutableLiveData<List<ListItemBinder>> getListDataLiveData() {
        return mListDataLiveData;
    }

    public MutableLiveData<Boolean> getStartSpeechAnimationLiveData() {
        return mStartSpeechAnimationLiveData;
    }

    public SingleLiveEvent<ExerciseType> getListElementClickLiveData() {
        return mListElementClickLiveData;
    }

    public SingleLiveEvent<Void> getInformationButtonClickLiveData() {
        return mInformationButtonClickLiveData;
    }

    /**
     * Пользователь нажал на кнопку старта аудиозаписи
     */
    public void onStartRecordAudioButtonClicked() {
        mAIManager.startListening();
    }

    public void onInformationButtonClicked() {
        mInformationButtonClickLiveData.postValue(null);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mAIManager.setListener(null);
    }

    private List<ListItemBinder> buildAdapterEntities(DataReadResponse dataReadResponse) {
        List<ListItemBinder> result = new ArrayList<>();

        result.add(buildSummaryAdapterEntity(dataReadResponse, ExerciseType.PUSH_UP));
        result.add(buildSummaryAdapterEntity(dataReadResponse, ExerciseType.PULL_UP));
        result.add(buildSummaryAdapterEntity(dataReadResponse, ExerciseType.PRESS));
        return result;
    }

    private MainScreenListItemBinder buildSummaryAdapterEntity(DataReadResponse dataReadResponse, ExerciseType type) {
        List<DataPoint> dataPoints = FitnessUtils.getDataPointsByType(dataReadResponse, type);
        int count = FitnessUtils.getSumOfIntegerValueFromDataPoints(dataPoints, Field.FIELD_REPETITIONS);
        Calendar from = CalendarUtils.getBeginningOfDayCalendar(Calendar.getInstance());
        Calendar to = CalendarUtils.getBeginningOfDayCalendar(Calendar.getInstance());
        to.add(Calendar.DAY_OF_MONTH, 1);
        int dayCount = FitnessUtils.getSumOfIntegerValueFromDataPoints(
                DataPointUtils.filterByDates(dataPoints, from, to),
                Field.FIELD_REPETITIONS);
        AbstractExersizeTypeHelper helper = ExerciseTypeUtils.getHelperByType(type);
        return new MainScreenListItemBinder(helper.getIcon(),
                helper.getName(),
                count,
                dayCount,
                helper.getFitnessDataType(),
                view -> mListElementClickLiveData.postValue(type));
    }

    private MainScreenListItemBinder buildSummaryAdapterEntityOfActivity(DataReadResponse dataReadResponse, ExerciseType type) {
        List<DataPoint> dataPoints = FitnessUtils.getDataPointsByType(dataReadResponse, type);
        int count = (int) FitnessUtils.getTotalTimeFromDataPoint(dataPoints);
        Calendar from = CalendarUtils.getBeginningOfDayCalendar(Calendar.getInstance());
        Calendar to = CalendarUtils.getBeginningOfDayCalendar(Calendar.getInstance());
        to.add(Calendar.DAY_OF_MONTH, 1);
        int dayCount = (int) FitnessUtils.getTotalTimeFromDataPoint(
                DataPointUtils.filterByDates(dataPoints, from, to));
        AbstractExersizeTypeHelper helper = ExerciseTypeUtils.getHelperByType(type);
        return new MainScreenListItemBinder(helper.getIcon(), helper.getName(),
                count,
                dayCount,
                helper.getFitnessDataType(),
                view -> mListElementClickLiveData.postValue(type));
    }

    /**
     * Слушатель обработчика речи
     */
    private class MainActivityAiListener implements AIListener {

        @Override
        public void onResult(AIResponse result) {
            mTTSManager.toSpeech(result.getResult().getFulfillment().getSpeech());
            mToastMessageLiveData.setValue(result.getResult().getFulfillment().getSpeech());
            ExerciseSet set = mAIParser.parse(result.getResult().getParameters());
            if (set.getType() != null) {
                mFitnessManager.writeData(set, aVoid -> {
                    Date from = CalendarUtils.getBeginningOfMonthCalendar(Calendar.getInstance()).getTime();
                    Date to = Calendar.getInstance().getTime();
                    mFitnessManager.readData(
                            Arrays.asList(ExerciseType.values()),
                            from,
                            to,
                            dataReadResponse -> mListDataLiveData.setValue(buildAdapterEntities(dataReadResponse)),
                            null);
                }, null);
            }
        }

        @Override
        public void onError(AIError error) {

        }

        @Override
        public void onAudioLevel(float level) {

        }

        @Override
        public void onListeningStarted() {
            mStartSpeechAnimationLiveData.setValue(true);
        }

        @Override
        public void onListeningCanceled() {
            mStartSpeechAnimationLiveData.setValue(false);
        }

        @Override
        public void onListeningFinished() {
            mStartSpeechAnimationLiveData.setValue(false);
        }
    }

    private class GoogleFitOnSuccessListener implements OnSuccessListener<DataReadResponse> {
        @Override
        public void onSuccess(DataReadResponse dataReadResponse) {
            mScreenStateLiveData.setValue(ScreenState.CONTENT);
            mListDataLiveData.setValue(buildAdapterEntities(dataReadResponse));
        }
    }

    private class GoogleFitOnErrorListener implements OnFailureListener {
        @Override
        public void onFailure(@NonNull Exception e) {
            mScreenStateLiveData.setValue(ScreenState.ERROR);
        }
    }
}
