package com.tripeo.fitassistant.mvp.model;

/**
 * @author Trikhin P O
 * @date 01.05.2018
 *
 * Класс соответствующий сету упражнения
 */

public class ExerciseSet {

    private ExerciseType mType;
    private int mRepetitions;
    /**
     * Длительность упражнения в миллисекундах
     */
    private long mDuration;

    public ExerciseType getType() {
        return mType;
    }

    public void setType(ExerciseType type) {
        mType = type;
    }

    public int getRepetitions() {
        return mRepetitions;
    }

    public void setRepetitions(int repetitions) {
        mRepetitions = repetitions;
    }

    public long getDuration() {
        return mDuration;
    }

    public void setDuration(long duration) {
        mDuration = duration;
    }
}
