package com.tripeo.fitassistant.mvp.viewmodel.factories;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.viewmodel.ExerciseActivityViewModel;

/**
 * Фабрика для создания {@link ExerciseActivityViewModel}
 */
public class ExerciseActivityViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private ExerciseType mExerciseType;
    private Application mApplication;

    public ExerciseActivityViewModelFactory(Application application, ExerciseType exerciseType) {
        super();
        mExerciseType = exerciseType;
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == ExerciseActivityViewModel.class) {
            return (T) new ExerciseActivityViewModel(mApplication, mExerciseType);
        }
        return super.create(modelClass);
    }
}
