package com.tripeo.fitassistant.mvp.view;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tripeo.fitassistant.mvp.view.fragments.MainFragmentsBundle;

/**
 * @author Trikhin P O
 */
public class MainViewPagerAdapter extends FragmentPagerAdapter {

    private MainFragmentsBundle mFragmentsBundle;

    public MainViewPagerAdapter(FragmentManager fm, MainFragmentsBundle fragmentsBundle) {
        super(fm);
        mFragmentsBundle = fragmentsBundle;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentsBundle.getFragmentByPosition(position);
    }

    @Override
    public int getCount() {
        return mFragmentsBundle.getNumberOfFragments();
    }
}
