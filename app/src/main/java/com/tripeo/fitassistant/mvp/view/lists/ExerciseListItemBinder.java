package com.tripeo.fitassistant.mvp.view.lists;

import android.view.View;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.google.android.gms.fitness.data.DataPoint;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.utils.ExerciseTypeUtils;

/**
 * Холдер соответствующий конкретному подходу в упражнении
 *
 * @author Trikhin P O on 21.10.2018
 */
public class ExerciseListItemBinder extends ListItemBinder<ExerciseListItemHolder> {

    private DataPoint mDataPoint;
    private boolean mIsLastItemInList;
    private ExerciseType mExerciseType;
    private View.OnClickListener mOnClickListener;

    public ExerciseListItemBinder(DataPoint dataPoint,
                                  ExerciseType exerciseType,
                                  boolean isLastItemInList,
                                  View.OnClickListener onClickListener) {
        super(R.layout.exercise_history_list_item);
        mDataPoint = dataPoint;
        mIsLastItemInList = isLastItemInList;
        mExerciseType = exerciseType;
        mOnClickListener = onClickListener;
    }

    @Override
    public ExerciseListItemHolder createCommonViewHolder(View view) {
        return new ExerciseListItemHolder(view, ExerciseTypeUtils.getHelperByType(mExerciseType));
    }

    public DataPoint getDataPoint() {
        return mDataPoint;
    }

    public boolean isLastItemInList() {
        return mIsLastItemInList;
    }

    public View.OnClickListener getOnClickListener() {
        return mOnClickListener;
    }
}
