package com.tripeo.fitassistant.mvp.viewmodel;

import android.app.Application;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.FitAssistantApplication;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.managers.interfaces.IPreferenceManager;
import com.tripeo.fitassistant.mvp.view.lists.SettingsListItemBinder;
import com.tripeo.fitassistant.mvp.viewmodel.framework.SingleLiveEvent;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * ViewModel соответствующая фрагменту настроек приложения
 *
 * @author Trikhin P O on 03.10.2018
 */
public class SettingsFragmentViewModel extends AndroidViewModel {

    private SingleLiveEvent<Void> mLogoutButtonLiveData;
    private SingleLiveEvent<Void> mConfirmLogoutLiveData;
    private SingleLiveEvent<DialogInterface> mDeclineLogoutLiveData;
    private SingleLiveEvent<Void> mInfoButtonLiveData;
    private MutableLiveData<List<ListItemBinder>> mListMutableLiveData;

    @Inject
    IPreferenceManager mPreferenceManager;

    public SettingsFragmentViewModel(@NonNull Application application) {
        super(application);
        ((FitAssistantApplication) application).getAppComponent().inject(this);
        mLogoutButtonLiveData = new SingleLiveEvent<>();
        mConfirmLogoutLiveData = new SingleLiveEvent<>();
        mDeclineLogoutLiveData = new SingleLiveEvent<>();
        mInfoButtonLiveData = new SingleLiveEvent<>();
        mListMutableLiveData = new MutableLiveData<>();
        mListMutableLiveData.postValue(generateListItems());
    }

    /**
     * Получение LiveData которая передает информацию о том была ли нажата кнопка сброса авторизации.
     */
    public SingleLiveEvent<Void> getLogoutButtonLiveData() {
        return mLogoutButtonLiveData;
    }

    /**
     * Получает LiveData которая передает информацию об элементах списка
     */
    public MutableLiveData<List<ListItemBinder>> getListMutableLiveData() {
        return mListMutableLiveData;
    }

    /**
     * Получает LiveData которая передает информацию о том что нажата кнопка с информацие о командах
     */
    public SingleLiveEvent<Void> getInfoButtonLiveData() {
        return mInfoButtonLiveData;
    }

    /**
     * Получает LiveData которая передает информацию о том что пользователь подтвердил смену регистрации
     */
    public SingleLiveEvent<Void> getConfirmLogoutLiveData() {
        return mConfirmLogoutLiveData;
    }

    /**
     * Получает LiveData которая передает информацию о том что пользователь отменил смену регистрации
     */
    public SingleLiveEvent<DialogInterface> getDeclineLogoutLiveData() {
        return mDeclineLogoutLiveData;
    }

    /**
     * Пользователь подтвердил сброс регистрации
     */
    public void onLogoutConfirmClick() {
        mConfirmLogoutLiveData.postValue(null);
    }

    /**
     * Пользователь отменил сброс регистрации
     */
    public void onLogoutDeclineClick(DialogInterface dialogInterface) {
        mDeclineLogoutLiveData.postValue(dialogInterface);
    }

    private List<ListItemBinder> generateListItems() {
        List<ListItemBinder> result = new ArrayList<>();
        result.add(new SettingsListItemBinder(
                R.drawable.ic_information_24dp,
                R.string.settings_info,
                view -> mInfoButtonLiveData.postValue(null)));
        result.add(new SettingsListItemBinder(
                R.drawable.ic_logout_24dp_black,
                R.string.settings_logout,
                view -> mLogoutButtonLiveData.postValue(null)));
        return result;
    }
}
