package com.tripeo.fitassistant.mvp.view.fragments;

import com.google.android.gms.fitness.data.DataPoint;

/**
 * Слушатель события что пользователь удалил один из своих подходов {@link com.google.android.gms.fitness.data.DataPoint}
 *
 * @author Trikhin P O
 */

public interface OnDataPointRemovedListener {

    /**
     * Метод оповещающий о том что был удален подход
     */
    void onDataPointRemoved(DataPoint dataPoint);

}
