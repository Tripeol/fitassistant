package com.tripeo.fitassistant.mvp.model;

/**
 * @author Trikhin P O
 * @since 01.05.2018
 *
 * Енум соответствующий типу упражнения
 */

public enum ExerciseType {

    /**
     * Отжимания
     */
    PUSH_UP,
    /**
     * Медитация
     */
    MEDITATION,
    /**
     * Планка
     */
    PLANK,
    /**
     * Подтягивания
     */
    PULL_UP,
    /**
     * Скручивания на пресс (всевозможные)
     */
    PRESS

}
