package com.tripeo.fitassistant.mvp.view.lists;

import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.R;

public class HistoryListItemBinder extends ListItemBinder<HistoryListItemHolder> {

    @DrawableRes
    private int mImageRes;
    @StringRes
    private int mTitleRes;
    private View.OnClickListener mOnClickListener;
    private boolean mIsLastItemInList;

    public HistoryListItemBinder(@DrawableRes int image, @StringRes int title, boolean isLastItemInList,  View.OnClickListener onClickListener) {
        super(R.layout.history_fragment_item_layout);
        mImageRes = image;
        mTitleRes = title;
        mIsLastItemInList = isLastItemInList;
        mOnClickListener = onClickListener;
    }

    public int getImageRes() {
        return mImageRes;
    }

    public int getTitleRes() {
        return mTitleRes;
    }

    public View.OnClickListener getOnClickListener() {
        return mOnClickListener;
    }

    public boolean isLastItemInList() {
        return mIsLastItemInList;
    }

    @Override
    public HistoryListItemHolder createCommonViewHolder(View view) {
        return new HistoryListItemHolder(view);
    }
}
