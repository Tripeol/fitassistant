package com.tripeo.fitassistant.mvp.view.lists;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.commonrecyclerviewadapter.ListItemHolder;
import com.google.android.gms.fitness.data.DataPoint;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Холдер соответствующий конкретному подходу в упражнении
 *
 * @author Trikhin P O on 21.10.2018
 */
public class ExerciseListItemHolder extends ListItemHolder<ExerciseListItemBinder> {

    private ViewGroup mRootTextView;
    private TextView mTimeTextView;
    private TextView mValueTextView;
    private View mDivider;
    private SimpleDateFormat mSimpleDateFormat;
    private AbstractExersizeTypeHelper mExersizeTypeHelper;

    public ExerciseListItemHolder(@NonNull View itemView, AbstractExersizeTypeHelper helper) {
        super(itemView);
        mRootTextView = itemView.findViewById(R.id.root_text_view);
        mTimeTextView = itemView.findViewById(R.id.time_text_view);
        mValueTextView = itemView.findViewById(R.id.value_text_view);
        mDivider = itemView.findViewById(R.id.divider_view);

        mSimpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        mExersizeTypeHelper = helper;
    }

    @Override
    public void bind(ExerciseListItemBinder exerciseListItemBinder) {
        mRootTextView.setOnClickListener(exerciseListItemBinder.getOnClickListener());

        List<DataPoint> dataPoints = new ArrayList<>();
        dataPoints.add(exerciseListItemBinder.getDataPoint());

        String timeAsString = mSimpleDateFormat.format(new Date(exerciseListItemBinder.getDataPoint().getStartTime(TimeUnit.MILLISECONDS)));
        mTimeTextView.setText(timeAsString);
        mValueTextView.setText(mExersizeTypeHelper.getCalculator().getTotalValueAsPrettyString(dataPoints));
        mDivider.setVisibility(exerciseListItemBinder.isLastItemInList() ? View.GONE : View.VISIBLE);
    }
}
