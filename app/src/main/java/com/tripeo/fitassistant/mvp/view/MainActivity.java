package com.tripeo.fitassistant.mvp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.customviews.NonSwipableViewPager;
import com.tripeo.fitassistant.mvp.view.fragments.HistoryFragment;
import com.tripeo.fitassistant.mvp.view.fragments.MainFragmentsBundle;
import com.tripeo.fitassistant.mvp.view.fragments.MainScreenFragment;
import com.tripeo.fitassistant.mvp.view.fragments.SettingsFragment;
import com.tripeo.fitassistant.mvp.viewmodel.MainActivityViewModel;

/**
 * {@link android.app.Activity} соответствующая главному экрану приложения
 */
public class MainActivity extends AppCompatActivity implements ISignOutActivity {

    private MainActivityViewModel mViewModel;

    private NonSwipableViewPager mViewPager;
    private MainFragmentsBundle mMainFragmentsBundle;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mViewModel.getGoogleSignOutLiveData().observe(this, aVoid -> navigateToSplashActivity());

        setupAppBar();
        setupViewPagerAndFragments();
        setupBottomBar();
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            mViewPager.setCurrentItem(0);
        }
    }

    private void navigateToSplashActivity() {
        startActivity(SplashActivity.newIntent(this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void signOut() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, task -> mViewModel.onGoogleSignOut());
    }

    private void setupAppBar() {
        setSupportActionBar(findViewById(R.id.toolbar));
    }

    private void setupBottomBar() {
        ((BottomNavigationView) findViewById(R.id.bottom_nav_bar)).setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.main_menu_main_button:
                    mViewPager.setCurrentItem(mMainFragmentsBundle.getPositionById(R.id.main_menu_main_button));
                    return true;
                case R.id.main_menu_settings_button:
                    mViewPager.setCurrentItem(mMainFragmentsBundle.getPositionById(R.id.main_menu_settings_button));
                    return true;
                case R.id.main_menu_history_button:
                    mViewPager.setCurrentItem(mMainFragmentsBundle.getPositionById(R.id.main_menu_history_button));
                    return true;
                default:
                    return false;
            }
        });
    }

    private void setupViewPagerAndFragments() {
        mViewPager = findViewById(R.id.view_pager);
        mMainFragmentsBundle = new MainFragmentsBundle();
        mMainFragmentsBundle.putFragment(R.id.main_menu_main_button, MainScreenFragment.newInstance());
        mMainFragmentsBundle.putFragment(R.id.main_menu_history_button, HistoryFragment.newInstance());
        mMainFragmentsBundle.putFragment(R.id.main_menu_settings_button, SettingsFragment.newInstance());
        mViewPager.setAdapter(new MainViewPagerAdapter(getSupportFragmentManager(), mMainFragmentsBundle));
        mViewPager.setOnTouchListener((view, motionEvent) -> true);
    }
}
