package com.tripeo.fitassistant.mvp.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.viewmodel.SplashActivityViewModel;

public class SplashActivity extends AppCompatActivity{

    private static final int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 3;
    private static final int REQUEST_AUDIO_PERMISSION_CODE = 4;

    SplashActivityViewModel mViewModel;

    public static Intent newIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(SplashActivityViewModel.class);
        mViewModel.getCheckGoogleFitAccessLiveData().observe(this, aVoid -> checkGoogleFitAccess());
        mViewModel.getCheckPermissionsLiveData().observe(this, aVoid -> checkPermissions());
        mViewModel.getNavigateToMainMenuLiveData().observe(this, aVoid -> navigateToMainMenu());
        mViewModel.getShowGoogleFitExplanationDialog().observe(this, aVoid -> showGoogleFitExplanationDialog());
        mViewModel.getShowPermissionExplanationDialog().observe(this, aVoid -> showPermissionExplanationDialog());

        mViewModel.tryToGetGoogleFitAccess();

        setContentView(R.layout.activity_splash);
    }

    private void checkGoogleFitAccess() {
        FitnessOptions fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE, FitnessOptions.ACCESS_WRITE)
                .build();

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this,
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            mViewModel.onGoogleFitAccessCheckedAndGranted();
        }
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_AUDIO_PERMISSION_CODE);
        } else {
            mViewModel.onPermissionsGranted();
        }
    }

    private void navigateToMainMenu() {
        Intent intent = MainActivity.newIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void showGoogleFitExplanationDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).
                setTitle(R.string.dialog_fit_access_explanation_title).
                setMessage(R.string.dialog_fit_access_explanation_body).
                setPositiveButton(R.string.dialog_fit_access_explanation_ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                }).create();
        dialog.show();
    }

    private void showPermissionExplanationDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).
                setTitle(R.string.dialog_permission_explanation_title).
                setMessage(R.string.dialog_permission_explanation_body).
                setPositiveButton(R.string.dialog_permission_explanation_ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                }).create();
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_AUDIO_PERMISSION_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mViewModel.onPermissionsGranted();
                } else {
                    mViewModel.onPermissionsDenied();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                mViewModel.onGoogleFitAccessCheckedAndGranted();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                mViewModel.onGoogleFitAccessDenied();
            }
        }

    }
}
