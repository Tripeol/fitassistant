package com.tripeo.fitassistant.mvp.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.view.lists.HistoryListItemBinder;
import com.tripeo.fitassistant.mvp.viewmodel.framework.SingleLiveEvent;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;
import com.tripeo.fitassistant.utils.ExerciseTypeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ViewModel соответствующая общему фрагменту истории, со списком упражнений
 *
 * @author Trikhin P O on 04.10.2018
 */
public class HistoryFragmentViewModel extends ViewModel {

    private MutableLiveData<List<ListItemBinder>> mExerciseListElementsLiveData;
    private SingleLiveEvent<ExerciseType> mListElementClickLiveData;


    public HistoryFragmentViewModel() {
        mExerciseListElementsLiveData = new MutableLiveData<>();
        mListElementClickLiveData = new SingleLiveEvent<>();

        mExerciseListElementsLiveData.setValue(getAllListBinders());
    }

    /**
     * Получить {@link LiveData} которая будет поставлять список упражнений для экрана
     */
    public LiveData<List<ListItemBinder>> getExerciseListElementsLiveData() {
        return mExerciseListElementsLiveData;
    }

    /**
     * Получить {@link LiveData} которая будет создавать события нажатия на элемент списка
     */
    public SingleLiveEvent<ExerciseType> getListElementClickLiveData() {
        return mListElementClickLiveData;
    }

    private List<ListItemBinder> getAllListBinders() {
        List<ListItemBinder> abstractViewModels = new ArrayList<>();
        for (int i = 0; i < ExerciseType.values().length; i ++) {
            boolean lastInList = i == ExerciseType.values().length - 1;
            AbstractExersizeTypeHelper abstractExersizeTypeHelper = ExerciseTypeUtils.getHelperByType(ExerciseType.values()[i]);
            int finalI = i;
            abstractViewModels.add(new HistoryListItemBinder(
                    abstractExersizeTypeHelper.getIcon(),
                    abstractExersizeTypeHelper.getName(),
                    lastInList,
                    view -> mListElementClickLiveData.setValue(ExerciseType.values()[finalI]))
            );
        }
        return abstractViewModels;
    }
}
