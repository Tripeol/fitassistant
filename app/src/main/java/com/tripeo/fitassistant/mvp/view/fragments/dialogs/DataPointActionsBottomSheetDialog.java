package com.tripeo.fitassistant.mvp.view.fragments.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.view.fragments.OnDataPointRemovedListener;
import com.tripeo.fitassistant.mvp.viewmodel.DataPointActionsDialogViewModel;
import com.tripeo.fitassistant.mvp.viewmodel.factories.DataPointBottomDialogViewModelFactory;

/**
 * Диалог с действиями для выбранного подхода упражнения
 *
 * @author Trikhin P O
 */
public class DataPointActionsBottomSheetDialog extends BottomSheetDialogFragment {

    public static String TAG = "DataPointActionsBottomSheetDialog";
    public static String DATA_POINT_KEY = "dataPoint";

    private DataPointActionsDialogViewModel mViewModel;
    private TextView mDeleteTextView;

    private DataPoint mDataPoint;

    public static DataPointActionsBottomSheetDialog newInstance(DataPoint dataPoint) {
        Bundle args = new Bundle();
        args.putParcelable(DATA_POINT_KEY, dataPoint);
        DataPointActionsBottomSheetDialog dialog = new DataPointActionsBottomSheetDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_bar_history_layout, container,
                false);
        mDeleteTextView = view.findViewById(R.id.delete_text_view);
        view.findViewById(R.id.edit_image_button).setVisibility(View.GONE);
        view.findViewById(R.id.edit_text_view).setVisibility(View.GONE);

        if (savedInstanceState != null) {
            decodeBundle(savedInstanceState);
        } else {
            decodeBundle(getArguments());
        }

        mViewModel = ViewModelProviders.of(this, new DataPointBottomDialogViewModelFactory(getActivity().getApplication(), mDataPoint))
                .get(DataPointActionsDialogViewModel.class);

        mViewModel.getOnDataPointRemovedLiveEvent().observe(this, this::onDataPointRemoved);

        setupViews();


        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(DATA_POINT_KEY, mDataPoint);
    }

    private void onDataPointRemoved(boolean isRemoveSuccess) {
        if(isRemoveSuccess) {
            if (getActivity() != null) {
                 ((OnDataPointRemovedListener)getActivity()).onDataPointRemoved(mDataPoint);
                 dismiss();
            }
        } else {
            Toast.makeText(getContext(), R.string.datapoint_removed_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void decodeBundle(@Nullable Bundle bundle) {
        if (bundle != null && bundle.containsKey(DATA_POINT_KEY)) {
            mDataPoint = bundle.getParcelable(DATA_POINT_KEY);
        }
    }

    private void setupViews() {
        mDeleteTextView.setOnClickListener(view -> mViewModel.onDeleteButtonClicked());
    }
}
