package com.tripeo.fitassistant.mvp.model;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.data.BarEntry;
import com.google.android.gms.fitness.data.DataPoint;

import java.util.List;

/**
 * Класс инкапсулирующий информацию о баре на графике.
 */
public class ExerciseBarEntry extends BarEntry {

    private List<DataPoint> mDataPoints;
    private long mEntryDate;

    /**
     * @param x          координата бара
     * @param y          высота бара
     * @param dataPoints упражнения относящиеся к данному бару (выполненные в промежутке к которому он относится)
     * @param date       время относящееся к бару (любой момент времени внутри промежутка к которому относится бар)
     */
    public ExerciseBarEntry(float x, float y, @NonNull List<DataPoint> dataPoints, long date) {
        super(x, y);
        mDataPoints = dataPoints;
        mEntryDate = date;
    }

    @NonNull
    public List<DataPoint> getDataPoints() {
        return mDataPoints;
    }

    public long getEntryDate() {
        return mEntryDate;
    }
}
