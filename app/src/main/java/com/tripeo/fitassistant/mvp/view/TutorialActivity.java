package com.tripeo.fitassistant.mvp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.NumberPicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.tripeo.fitassistant.R;

public class TutorialActivity extends AppCompatActivity {

    private NumberPicker mNumberPicker;
    private AppCompatSpinner mSpinner;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, TutorialActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        mNumberPicker = findViewById(R.id.number_picker);
        mNumberPicker.setMinValue(0);
        mNumberPicker.setMaxValue(2);
        mNumberPicker.setDisplayedValues(new String[]{"1", "13", "261"});

        mSpinner = findViewById(R.id.spinner_view);
       // mSpinner.setAdapter(new ArrayAdapter<String>());
    }
}
