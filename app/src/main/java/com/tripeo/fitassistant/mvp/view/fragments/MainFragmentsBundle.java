package com.tripeo.fitassistant.mvp.view.fragments;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Класс содержащий фрагменты отображаемые на главной в {@link ViewPager}
 *
 * @author Trikhin P O
 */
public class MainFragmentsBundle {

    private Map<Integer, Fragment> mFragments;

    public MainFragmentsBundle() {
        mFragments = new LinkedHashMap<>();
    }

    /**
     * @param id       идентификатор фрагмента
     * @param fragment фрагмент
     */
    public void putFragment(int id, Fragment fragment) {
        mFragments.put(id, fragment);
    }

    /**
     * Возвращает порядковый номер по идентификатору
     *
     * @return позиция элеменета с указанным идентификатором,
     * -1 если такой элемент не найден
     */
    public int getPositionById(int id) {
        int i = -1;
        for (Map.Entry<Integer, Fragment> entry : mFragments.entrySet()) {
            i++;
            if (entry.getKey() == id) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Возвращает фрагмент по идентификатору
     */
    public Fragment getFragmentById(int id) {
        return mFragments.get(id);
    }

    /**
     * Возвращает фрагмент по порядковому номеру
     *
     * @return фрагмент с указанным порядковым номером либо null
     */
    @Nullable
    public Fragment getFragmentByPosition(int pos) {
        int i = -1;
        for (Map.Entry<Integer, Fragment> entry : mFragments.entrySet()) {
            i++;
            if (i == pos) {
                return entry.getValue();
            }
        }
        return null;
    }

    public int getNumberOfFragments() {
        return mFragments.size();
    }

}
