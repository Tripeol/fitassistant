package com.tripeo.fitassistant.mvp.view;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Енум отвечающий за состояние экрана
 */
@Retention(RetentionPolicy.SOURCE)
@IntDef({ScreenState.LOADING, ScreenState.CONTENT, ScreenState.ERROR})
public @interface ScreenState {
    /**
     * Состояние загрузки
     */
    static final int LOADING = 1;
    /**
     * Состояние отображения контента
     */
    static final int CONTENT = 2;
    /**
     * Состояние ошибки
     */
    static final int ERROR = 3;

}
