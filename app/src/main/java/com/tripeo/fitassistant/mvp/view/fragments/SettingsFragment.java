package com.tripeo.fitassistant.mvp.view.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.example.commonrecyclerviewadapter.CommonAdapter;
import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.FitAssistantApplication;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.view.ISignOutActivity;
import com.tripeo.fitassistant.mvp.viewmodel.SettingsFragmentViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private CommonAdapter mCommonAdapter;

    private SettingsFragmentViewModel mViewModel;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((FitAssistantApplication) context.getApplicationContext()).getAppComponent().inject(this);
        mViewModel = ViewModelProviders.of(this).get(SettingsFragmentViewModel.class);
        mViewModel.getListMutableLiveData().observe(this, this::setListItems);
        mViewModel.getInfoButtonLiveData().observe(this, aVoid -> openInfoDialog());
        mViewModel.getConfirmLogoutLiveData().observe(this, aVoid -> logout());
        mViewModel.getDeclineLogoutLiveData().observe(this, DialogInterface::dismiss);
        mViewModel.getLogoutButtonLiveData().observe(this, aVoid -> showLogoutDialog());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_settings, container, false);
        mRecyclerView = fragmentView.findViewById(R.id.recycler_view);
        mCommonAdapter = new CommonAdapter();
        mRecyclerView.setAdapter(mCommonAdapter);
        return fragmentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewModel.getLogoutButtonLiveData().removeObservers(this);
    }

    private void setListItems(List<ListItemBinder> listItems) {
        mCommonAdapter.setBinders(listItems);
    }

    private void showLogoutDialog() {
        if (getActivity() != null) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.dialog_change_user_title)
                    .setMessage(R.string.dialog_change_user_body)
                    .setPositiveButton(R.string.ok, (dialogInterface, i) -> mViewModel.onLogoutConfirmClick())
                    .setNegativeButton(R.string.cancel, (dialogInterface, i) -> mViewModel.onLogoutDeclineClick(dialogInterface))
                    .show();
        }
    }

    private void logout() {
        if (getActivity() != null) {
            ((ISignOutActivity) getActivity()).signOut();
        }
    }

    private void openInfoDialog() {

    }
}
