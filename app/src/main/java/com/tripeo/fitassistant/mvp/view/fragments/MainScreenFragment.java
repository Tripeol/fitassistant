package com.tripeo.fitassistant.mvp.view.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.commonrecyclerviewadapter.CommonAdapter;
import com.example.commonrecyclerviewadapter.ListItemBinder;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.view.ExerciseActivity;
import com.tripeo.fitassistant.mvp.view.ScreenState;
import com.tripeo.fitassistant.mvp.viewmodel.MainFragmentViewModel;

import java.util.List;

/**
 * Фрагмент содержащий UI главной страницы приложения
 *
 * @author Trikhin P O
 */
public class MainScreenFragment extends Fragment {

    private MainFragmentViewModel mMainFragmentViewModel;

    private RecyclerView mRecyclerView;
    private CommonAdapter mAdapter;
    private ViewGroup mContentLayout;
    private ViewGroup mErrorLayout;
    private ProgressBar mLoadingLayout;
    private TextView mDevModeTextView;
    private ImageButton mStartRecordImageButton;
    private ImageView mInformationButton;

    private Animation mScaleAnimation;

    public static MainScreenFragment newInstance() {
        return new MainScreenFragment();
    }

    public MainScreenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mMainFragmentViewModel = ViewModelProviders.of(this).get(MainFragmentViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mMainFragmentViewModel.getToastMessageLiveData().observe(this, message -> showToastMessage(message));
        mMainFragmentViewModel.getScreenStateLiveData().observe(this, screenState -> setScreenState(screenState));
        mMainFragmentViewModel.getListDataLiveData().observe(this, data -> updateListData(data));
        mMainFragmentViewModel.getStartSpeechAnimationLiveData().observe(this, shouldStart -> startSpeechAnimation(shouldStart));
        mMainFragmentViewModel.getListElementClickLiveData().observe(this, type -> onElementClicked(type));
        mMainFragmentViewModel.getInformationButtonClickLiveData().observe(this, aVoid -> openInformationFragment());

        View fragmentView = inflater.inflate(R.layout.fragment_main_screen, container, false);
        findViews(fragmentView);

        mStartRecordImageButton.setOnClickListener(view -> mMainFragmentViewModel.onStartRecordAudioButtonClicked());
        mInformationButton.setOnClickListener(view -> mMainFragmentViewModel.onInformationButtonClicked());
        configureRecyclerView(fragmentView);
        setupAnimations();

        return fragmentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMainFragmentViewModel.getToastMessageLiveData().removeObservers(this);
        mMainFragmentViewModel.getScreenStateLiveData().removeObservers(this);
        mMainFragmentViewModel.getListDataLiveData().removeObservers(this);
        mMainFragmentViewModel.getStartSpeechAnimationLiveData().removeObservers(this);
    }

    private void updateListData(List<ListItemBinder> entityList) {
        mAdapter.setBinders(entityList);
    }

    private void setScreenState(int state) {
        switch (state) {
            case ScreenState.CONTENT:
                mContentLayout.setVisibility(View.VISIBLE);
                mLoadingLayout.setVisibility(View.GONE);
                mErrorLayout.setVisibility(View.GONE);
                break;
            case ScreenState.ERROR:
                mContentLayout.setVisibility(View.GONE);
                mLoadingLayout.setVisibility(View.GONE);
                mErrorLayout.setVisibility(View.VISIBLE);
                break;
            case ScreenState.LOADING:
                mContentLayout.setVisibility(View.GONE);
                mLoadingLayout.setVisibility(View.VISIBLE);
                mErrorLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void startSpeechAnimation(boolean shouldStart) {
        if (shouldStart) {
            mStartRecordImageButton.startAnimation(mScaleAnimation);
        } else {
            mStartRecordImageButton.clearAnimation();
        }
    }

    private void onElementClicked(ExerciseType exerciseType) {
        startActivity(ExerciseActivity.newIntent(getContext(), exerciseType));
    }

    private void openInformationFragment() {
        if (getContext() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.tutorial_title);
            builder.setMessage(R.string.tutorial_body);
            builder.setPositiveButton(R.string.tutorial_positive_button, (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
        }
    }

    private void findViews(View fragmentView) {
        mContentLayout = fragmentView.findViewById(R.id.content_layout);
        mErrorLayout = fragmentView.findViewById(R.id.error_layout);
        mLoadingLayout = fragmentView.findViewById(R.id.progress_bar);
        mStartRecordImageButton = fragmentView.findViewById(R.id.start_record_button);
        mInformationButton = fragmentView.findViewById(R.id.information_button);
    }

    private void configureRecyclerView(View fragmentView) {
        mRecyclerView = fragmentView.findViewById(R.id.recycler_view);
        mAdapter = new CommonAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void setupAnimations() {
        mScaleAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_animation);
    }
}
