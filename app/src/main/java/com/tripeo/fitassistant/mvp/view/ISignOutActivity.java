package com.tripeo.fitassistant.mvp.view;

/**
 * {@link android.app.Activity} которая умеет сбрасывать регистрацию.
 *
 * @author Trikhin P O on 06.10.2018
 */
public interface ISignOutActivity {

    /**
     * Сбросить регистрацию
     */
    void signOut();

}
