package com.tripeo.fitassistant.mvp.viewmodel.factories;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.fitness.data.DataPoint;
import com.tripeo.fitassistant.mvp.viewmodel.DataPointActionsDialogViewModel;

/**
 * Фабрика для создания {@link DataPointActionsDialogViewModel}
 * @author Trikhin P O
 */
public class DataPointBottomDialogViewModelFactory  extends ViewModelProvider.NewInstanceFactory {

    private DataPoint mDataPoint;
    private Application mApplication;

    public DataPointBottomDialogViewModelFactory(Application application, DataPoint dataPoint) {
        super();
        mDataPoint = dataPoint;
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == DataPointActionsDialogViewModel.class) {
            return (T) new DataPointActionsDialogViewModel(mApplication, mDataPoint);
        }
        return super.create(modelClass);
    }
}
