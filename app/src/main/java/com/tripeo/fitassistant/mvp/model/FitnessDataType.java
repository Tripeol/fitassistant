package com.tripeo.fitassistant.mvp.model;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Аннотация соответствующая типу данных внутри {@link com.google.android.gms.fitness.data.DataPoint}
 *
 * @author Trikhin P O on 17.10.2018
 */
@Retention(RetentionPolicy.SOURCE)
@IntDef({FitnessDataType.DECIMAL, FitnessDataType.TIME})
public @interface FitnessDataType {

    int DECIMAL = 1;

    int TIME = 2;
}
