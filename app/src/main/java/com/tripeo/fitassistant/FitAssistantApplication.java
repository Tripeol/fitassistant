package com.tripeo.fitassistant;

import android.app.Application;

import com.tripeo.fitassistant.dagger.AIModule;
import com.tripeo.fitassistant.dagger.CommonManagersModule;
import com.tripeo.fitassistant.dagger.IAppComponent;
import com.tripeo.fitassistant.dagger.CoreModule;
import com.tripeo.fitassistant.dagger.DaggerIAppComponent;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 */

public class FitAssistantApplication extends Application {

    private IAppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (mAppComponent == null) {
            mAppComponent = DaggerIAppComponent.builder().
                    coreModule(new CoreModule(this)).
                    aIModule(new AIModule()).
                    commonManagersModule(new CommonManagersModule()).
                    build();
        }
    }

    public IAppComponent getAppComponent() {
        return mAppComponent;
    }
}
