package com.tripeo.fitassistant.parsers.exersizetypeparsers;

import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators.TimeExerciseCalculator;

import java.util.concurrent.TimeUnit;

/**
 * @author Trikhin P O
 * @since 05.05.2018
 * <p>
 * Класс содержащий методы для обработки медитации
 */

public class MeditationHelper extends AbstractExersizeTypeHelper {

    private static final String PARSE_KEY = "медитаци";

    public MeditationHelper() {
        super(ExerciseType.MEDITATION, new TimeExerciseCalculator());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getFitnessDataType() {
        return FitnessDataType.TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getName() {
        return R.string.meditation_exercise;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean parseDialogFlowKey(String value) {
        if (value.split(" ").length > 1) {
            return false;
        } else if (value.contains(PARSE_KEY)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean parseGoogleFitDataPoint(DataPoint dataPoint) {
        return dataPoint.getValue(Field.FIELD_ACTIVITY).asActivity().equals(FitnessActivities.MEDITATION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DataType getDataType() {
        return DataType.TYPE_ACTIVITY_SEGMENT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getWorkoutTime(ExerciseSet exerciseSet) {
        return (int) exerciseSet.getDuration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIcon() {
        return R.drawable.ic_meditation_black_24dp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DataPoint constructDataPointInternal(DataPoint dataPoint, ExerciseSet exerciseSet) {
        dataPoint.getValue(Field.FIELD_ACTIVITY).setActivity(FitnessActivities.MEDITATION);
        dataPoint.setTimestamp(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        return dataPoint;
    }
}
