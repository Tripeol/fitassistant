package com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators;

import com.google.android.gms.fitness.data.DataPoint;
import com.tripeo.fitassistant.utils.FitnessUtils;
import com.tripeo.fitassistant.utils.TimeUtils;

import java.util.List;

public class TimeExerciseCalculator extends AbstractExerciseCalculator {

    /**
     * {@inheritDoc}
     * Выдает время измеренное в секундах
     */
    @Override
    public long getTotalValue(List<DataPoint> dataPoints) {
        return FitnessUtils.getTotalTimeFromDataPoint(dataPoints)/1000;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTotalValueAsPrettyString(List<DataPoint> dataPoints) {
        return TimeUtils.getTimeIntervalAsString(getTotalValue(dataPoints)*1000);
    }
}
