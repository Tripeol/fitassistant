package com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators;

import com.google.android.gms.fitness.data.DataPoint;

import java.util.List;

/**
 * Класс для подсчета значений отображаемых пользователю в зависимости от типа упражнения.
 */
public abstract class AbstractExerciseCalculator {

    /**
     * Подсчитывает суммарное значение упражнения
     */
    public abstract long getTotalValue(List<DataPoint> dataPoints);

    /**
     * Подсчитывает суммарное значение и переводит его в красивую строку
     */
    public abstract String getTotalValueAsPrettyString(List<DataPoint> dataPoints);
}
