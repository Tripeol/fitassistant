package com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.WorkoutExercises;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;

import java.util.concurrent.TimeUnit;

/**
 * {@link AbstractExersizeTypeHelper} для всевозможных скручиваний на пресс
 *
 * @author Trikhin P O on 28.11.2018
 */
public class PressHelper extends AbstractExersizeTypeHelper {

    private static final String PRESS_GOOGLE_FIT_KEY = WorkoutExercises.CRUNCH;
    private static final String PRESS_PARSE_KEY = ".*скручиван.*на.*прес.*";
    private static final float PRESS_TIME_PER_UNIT = 600;

    public PressHelper() {
        super(ExerciseType.PRESS, new DecimalExerciseCalculator());
    }

    @Override
    public int getFitnessDataType() {
        return FitnessDataType.DECIMAL;
    }

    @Override
    public int getName() {
        return R.string.press_exercise;
    }

    @Override
    public boolean parseDialogFlowKey(String value) {
        if (value.matches(PRESS_PARSE_KEY)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean parseGoogleFitDataPoint(DataPoint dataPoint) {
        return PRESS_GOOGLE_FIT_KEY.equals(dataPoint.getValue(Field.FIELD_EXERCISE).asString());
    }

    @Override
    public DataType getDataType() {
        return DataType.TYPE_WORKOUT_EXERCISE;
    }

    @Override
    public int getWorkoutTime(ExerciseSet exerciseSet) {
        return (int) (exerciseSet.getRepetitions() * PRESS_TIME_PER_UNIT);
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_crunch_black_24dp;
    }

    @Override
    protected DataPoint constructDataPointInternal(DataPoint dataPoint, ExerciseSet exerciseSet) {
        dataPoint.getValue(Field.FIELD_REPETITIONS).setInt(exerciseSet.getRepetitions());
        dataPoint.getValue(Field.FIELD_EXERCISE).setString(PRESS_GOOGLE_FIT_KEY);
        dataPoint.getValue(Field.FIELD_RESISTANCE_TYPE).setInt(Field.RESISTANCE_TYPE_BODY);
        dataPoint.setTimestamp(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        return dataPoint;
    }
}
