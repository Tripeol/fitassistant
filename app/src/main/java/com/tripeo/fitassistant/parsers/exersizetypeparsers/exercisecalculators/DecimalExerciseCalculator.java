package com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.Field;
import com.tripeo.fitassistant.utils.FitnessUtils;

import java.util.List;

/**
 * {@link AbstractExerciseCalculator} для подсчета целых значений единиц упражнения, например для подсчета числа подходов.
 */
public class DecimalExerciseCalculator extends AbstractExerciseCalculator {

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalValue(List<DataPoint> dataPoints) {
        return FitnessUtils.getSumOfIntegerValueFromDataPoints(dataPoints, Field.FIELD_REPETITIONS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTotalValueAsPrettyString(List<DataPoint> dataPoints) {
        return String.valueOf(getTotalValue(dataPoints));
    }

}
