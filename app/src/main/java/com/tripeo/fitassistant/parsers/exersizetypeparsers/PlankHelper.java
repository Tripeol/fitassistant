package com.tripeo.fitassistant.parsers.exersizetypeparsers;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.WorkoutExercises;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators.TimeExerciseCalculator;

import java.util.concurrent.TimeUnit;

/**
 * Класс наследник {@link AbstractExersizeTypeHelper} который отвечает за работу с упражнением "планка".
 *
 * @author Trikhin P O on 09.07.2018
 */
public class PlankHelper extends AbstractExersizeTypeHelper {

    public static final String PLANK_GOOGLE_FIT_KEY = WorkoutExercises.PLANK;
    private static final String PARSE_KEY = "планк";

    public PlankHelper() {
        super(ExerciseType.PLANK, new TimeExerciseCalculator());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getFitnessDataType() {
        return FitnessDataType.TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getName() {
        return R.string.plank_exercise;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean parseDialogFlowKey(String value) {
        if (value.split(" ").length > 1) {
            return false;
        } else if (value.contains(PARSE_KEY)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean parseGoogleFitDataPoint(DataPoint dataPoint) {
        return PLANK_GOOGLE_FIT_KEY.equals(dataPoint.getValue(Field.FIELD_EXERCISE).asString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DataType getDataType() {
        return DataType.TYPE_WORKOUT_EXERCISE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getWorkoutTime(ExerciseSet exerciseSet) {
        return (int) exerciseSet.getDuration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIcon() {
        return R.drawable.ic_plank_black_24dp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataPoint constructDataPointInternal(DataPoint dataPoint, ExerciseSet exerciseSet) {
        dataPoint.getValue(Field.FIELD_EXERCISE).setString(PLANK_GOOGLE_FIT_KEY);
        dataPoint.setTimestamp(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        return dataPoint;
    }
}
