package com.tripeo.fitassistant.parsers.exersizetypeparsers;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators.AbstractExerciseCalculator;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * @author Trikhin P O
 * @date 01.05.2018
 * <p>
 * Абстрактный класс содержащий методы для обработки конкретного типа упражнения {@link ExerciseType}
 */

public abstract class AbstractExersizeTypeHelper {

    private ExerciseType mType;
    private AbstractExerciseCalculator mAbstractExerciseCalculator;

    public AbstractExersizeTypeHelper(ExerciseType type, AbstractExerciseCalculator calculator) {
        mType = type;
        mAbstractExerciseCalculator = calculator;
    }

    public DataPoint constructDataPoint(DataSet dataSet, ExerciseSet exerciseSet) {
        Calendar cal = Calendar.getInstance();
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.MILLISECOND, -(getWorkoutTime(exerciseSet)));
        long startTime = cal.getTimeInMillis();
        DataPoint dataPoint = dataSet.createDataPoint().setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
        dataPoint = constructDataPointInternal(dataPoint, exerciseSet);
        return dataPoint;
    }

    /**
     * Возвращает {@link ExerciseType} соответствующий данному хелперу
     */
    public ExerciseType getType() {
        return mType;
    }

    /**
     * Возвращает {@link AbstractExerciseCalculator} для подсчета количества выполнений данного упражнения
     */
    public AbstractExerciseCalculator getCalculator() {
        return mAbstractExerciseCalculator;
    }

    /**
     * Возвращает {@link FitnessDataType} соответствующий данному хелперу
     * Имеется ввиду что измеряемым значением данного типа является время, число (количество подходов) или что то другое.
     */
    @FitnessDataType
    public abstract int getFitnessDataType();

    /**
     * Возвращает название упражнения
     */
    @StringRes
    public abstract int getName();

    /**
     * Используется при парсинге сообщения от DialogFlow.
     * От сервера приходит значение value по которому нужно понять ок каком типе упражнения идет речь.
     * <p>
     * Должен вернуть true/false считает ли данный хелпер что данные с типом value относятся именно к его {@link ExerciseType}
     *
     * @return должен вернуть true если текущий хелпер соответствует входной строке
     */
    public abstract boolean parseDialogFlowKey(String value);

    /**
     * Используется при парсинге ответа от GoogleFit
     * Должен вернуть true/false считает ли данный хелпер что данные относятся именно к его {@link ExerciseType}
     *
     * @return должен вернуть true если текущий хелпер соответствует входным данным
     */
    public abstract boolean parseGoogleFitDataPoint(DataPoint dataPoint);

    /**
     * Должен вернуть {@link DataType} - тип активности в контексте GoogleFit для данного сета упражнений
     */
    public abstract DataType getDataType();

    /**
     * Должен вернуть количество времени которое занял переданный подход
     * измеряется в миллисекундах
     */
    public abstract int getWorkoutTime(ExerciseSet exerciseSet);

    /**
     * Возвращает ресурс иконки для данного упражнения
     */
    @DrawableRes
    public abstract int getIcon();

    /**
     * Конструирует {@link DataPoint} для упаковки и отправки в GoogleFit нужно проставить все поля кроме временного интервала
     */
    protected abstract DataPoint constructDataPointInternal(DataPoint dataPoint, ExerciseSet exerciseSet);


}
