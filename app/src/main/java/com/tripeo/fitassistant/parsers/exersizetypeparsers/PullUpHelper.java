package com.tripeo.fitassistant.parsers.exersizetypeparsers;


import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.WorkoutExercises;
import com.tripeo.fitassistant.R;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.mvp.model.FitnessDataType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators.DecimalExerciseCalculator;

import java.util.concurrent.TimeUnit;

/**
 * {@link AbstractExersizeTypeHelper} соответствующий упражнению: подтягивания
 *
 * @author Trikhin P O on 28.11.2018
 */
public class PullUpHelper extends AbstractExersizeTypeHelper {

    private static final String PULL_UP_GOOGLE_FIT_KEY = WorkoutExercises.PULLUP;
    private static final String PULL_UP_PARSE_KEY = "подтягиван";
    private static final float PULL_UP_TIME_PER_UNIT = 600;

    public PullUpHelper() {
        super(ExerciseType.PULL_UP, new DecimalExerciseCalculator());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getFitnessDataType() {
        return FitnessDataType.DECIMAL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getName() {
        return R.string.pull_up_exercise;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean parseDialogFlowKey(String value) {
        if (value.split(" ").length > 1) {
            return false;
        } else if (value.contains(PULL_UP_PARSE_KEY)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean parseGoogleFitDataPoint(DataPoint dataPoint) {
        return PULL_UP_GOOGLE_FIT_KEY.equals(dataPoint.getValue(Field.FIELD_EXERCISE).asString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DataType getDataType() {
        return DataType.TYPE_WORKOUT_EXERCISE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getWorkoutTime(ExerciseSet exerciseSet) {
        return (int) (exerciseSet.getRepetitions() * PULL_UP_TIME_PER_UNIT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIcon() {
        return R.drawable.ic_pullup_black_24dp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataPoint constructDataPointInternal(DataPoint dataPoint, ExerciseSet exerciseSet) {
        dataPoint.getValue(Field.FIELD_REPETITIONS).setInt(exerciseSet.getRepetitions());
        dataPoint.getValue(Field.FIELD_EXERCISE).setString(PULL_UP_GOOGLE_FIT_KEY);
        dataPoint.getValue(Field.FIELD_RESISTANCE_TYPE).setInt(Field.RESISTANCE_TYPE_BODY);
        dataPoint.setTimestamp(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        return dataPoint;
    }
}
