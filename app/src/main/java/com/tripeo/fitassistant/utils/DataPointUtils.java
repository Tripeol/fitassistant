package com.tripeo.fitassistant.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.fitness.data.DataPoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Утилитный класс для работы с {@link com.google.android.gms.fitness.data.DataPoint}
 *
 * @author Trikhin P O on 07.06.2018
 */
public final class DataPointUtils {

    private static String DEBUG_POSTFIX = "debug";

    private DataPointUtils() {
        throw new IllegalArgumentException("This class shouldnt be instanciated");
    }

    /**
     * Фильтрует список {@link DataPoint} по дате, если какая либо дата указана как null то не фильтруем по этой дате.
     * Нижняя гранциа включительна, верхняя не включительна
     *
     * @param dataPoints список {@link DataPoint} для фильтра
     * @param from       нижняя граница фильтра
     * @param to         верхняя граница фильтра
     */
    @NonNull
    public static List<DataPoint> filterByDates(@Nullable List<DataPoint> dataPoints, @Nullable Calendar from, @Nullable Calendar to) {
        if (dataPoints == null || dataPoints.isEmpty()) {
            return new ArrayList<>();
        }
        if (from == null && to == null) {
            return dataPoints;
        }

        List<DataPoint> result = new ArrayList<>();
        for (DataPoint dataPoint : dataPoints) {
            boolean passedFrom = (from != null && dataPoint.getStartTime(TimeUnit.MILLISECONDS) > from.getTimeInMillis()) || (from == null);
            boolean passedTo = (to != null && dataPoint.getEndTime(TimeUnit.MILLISECONDS) < to.getTimeInMillis()) || (to == null);
            if (passedFrom && passedTo) {
                result.add(dataPoint);
            }
        }

        return result;
    }

}
