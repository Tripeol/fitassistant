package com.tripeo.fitassistant.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.PushUpHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author Trikhin P O
 * @since 29.04.2018
 * <p>
 * Утилитный класс для облегчения работы с GoogleFit
 * Например содержит методы для красивого вывода данных.
 */

public class FitnessUtils {

    /**
     * Выводит {@link DataReadResponse} строку удобную для чтения
     */
    public static String dataReadResponseToString(DataReadResponse dataReadResponse) {
        StringBuilder stringBuilder = new StringBuilder();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss dd:MM:yyyy", Locale.ENGLISH);

        for (DataSet dataSet : dataReadResponse.getDataSets()) {
            for (DataPoint dp : dataSet.getDataPoints()) {
                stringBuilder.append("Data point:\n");
                stringBuilder.append("\tType: ").append(dp.getDataType().getName()).append("\n");
                stringBuilder.append("\tStart: ").append(dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS))).append("\n");
                stringBuilder.append("\tEnd: ").append(dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS))).append("\n");
                for (Field field : dp.getDataType().getFields()) {
                    stringBuilder.append("\tField: ").append(field.getName()).append(" Value: ").append(dp.getValue(field)).append("\n");
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Парсит {@link DataReadResponse} и возвращает содержащуюся в нем сумму отжиманий
     */
    public static int getPushUpCount(DataReadResponse dataReadResponse) {
        int result = 0;
        for (DataSet dataSet : dataReadResponse.getDataSets()) {
            for (DataPoint dp : dataSet.getDataPoints()) {
                if (PushUpHelper.PUSH_UP_GOOGLE_FIT_KEY.equals(dp.getValue(Field.FIELD_EXERCISE).asString())) {
                    result = result + dp.getValue(Field.FIELD_REPETITIONS).asInt();
                }
            }
        }

        return result;
    }

    /**
     * Парсит {@link DataReadResponse} и возвращает список {@link DataPoint} совпадающих по типу с параметром {@code exerciseType}
     */
    @NonNull
    public static List<DataPoint> getDataPointsByType(@Nullable DataReadResponse dataReadResponse,
                                                      @NonNull ExerciseType exerciseType) {
        if (dataReadResponse == null || dataReadResponse.getDataSets() == null) {
            return new ArrayList<>();
        }

        List<DataPoint> result = new ArrayList<>();
        for (DataSet dataSet : dataReadResponse.getDataSets()) {
            AbstractExersizeTypeHelper exersizeTypeHelper = ExerciseTypeUtils.getHelperByType(exerciseType);
            if (dataSet.getDataType().equals(exersizeTypeHelper.getDataType())) {
                for (DataPoint dataPoint : dataSet.getDataPoints()) {
                    if (exersizeTypeHelper.parseGoogleFitDataPoint(dataPoint)) {
                        result.add(dataPoint);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Возвращает сумму значений в полях {@link Field} для переданного типа упражнения {@link ExerciseType}
     */
    public static int getSumOfIntegerValueFromDataPoints(@Nullable List<DataPoint> dataPoints, @NonNull Field field) {
        if (dataPoints == null || dataPoints.isEmpty()) {
            return 0;
        }

        int result = 0;
        for (DataPoint dataPoint : dataPoints) {
            result = result + dataPoint.getValue(field).asInt();
        }
        return result;
    }

    /**
     * Возвращает суммарное время всех переданных {@link DataPoint}
     */
    public static long getTotalTimeFromDataPoint(@Nullable List<DataPoint> dataPoints) {
        if (dataPoints == null || dataPoints.isEmpty()) {
            return 0;
        }

        long result = 0;
        for (DataPoint dataPoint : dataPoints) {
            result = result + dataPoint.getEndTime(TimeUnit.MILLISECONDS) - dataPoint.getStartTime(TimeUnit.MILLISECONDS);
        }
        return result;
    }
}
