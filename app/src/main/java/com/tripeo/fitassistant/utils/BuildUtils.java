package com.tripeo.fitassistant.utils;

import com.tripeo.fitassistant.BuildConfig;

/**
 * Утилитный класс для работы с артефактами билда
 *
 * @author Trikhin P O on 23.06.2018
 */
public class BuildUtils {

    /**
     * Является ли данная сборка дебажной или релизной.
     */
    public static boolean isDebug () {
        return BuildConfig.DEBUG;
    }

}
