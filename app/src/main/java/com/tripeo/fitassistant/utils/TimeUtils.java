package com.tripeo.fitassistant.utils;

import android.text.format.DateUtils;

/**
 * Утилитный класс для работы со временем в различных проявлениях
 *
 * @author Trikhin P O on 03.07.2018
 */
public class TimeUtils {

    /**
     * Возвращает красивую строку описывающую интервал
     * Пример 21д 12ч 22мин 13 сек
     *
     * @param duration длительность интервала
     */
    public static String getTimeIntervalAsString(long duration) {
        long partialDuration = duration;

        int yearCount = (int) (duration / DateUtils.YEAR_IN_MILLIS);
        partialDuration = duration - yearCount * DateUtils.YEAR_IN_MILLIS;

        int dayCount = (int) (partialDuration / DateUtils.DAY_IN_MILLIS);
        partialDuration = partialDuration - dayCount * DateUtils.DAY_IN_MILLIS;

        int hourCount = (int) (partialDuration / DateUtils.HOUR_IN_MILLIS);
        partialDuration = partialDuration - hourCount * DateUtils.HOUR_IN_MILLIS;

        int minCount = (int) (partialDuration / DateUtils.MINUTE_IN_MILLIS);
        partialDuration = partialDuration - minCount * DateUtils.MINUTE_IN_MILLIS;

        int secCount = (int) (partialDuration / DateUtils.SECOND_IN_MILLIS);

        StringBuilder stringBuilder = new StringBuilder();
        if (yearCount > 0) {
            stringBuilder.append(yearCount).append("г.").append(" ");
        }
        if (yearCount > 0 || dayCount > 0) {
            stringBuilder.append(dayCount).append("д.").append(" ");
        }
        if (yearCount > 0 || dayCount > 0 || hourCount > 0) {
            stringBuilder.append(hourCount).append("ч.").append(" ");
        }
        if (yearCount > 0 || dayCount > 0 || hourCount > 0 || minCount > 0) {
            stringBuilder.append(minCount).append("мин.").append(" ");
        }
        if (yearCount > 0 || dayCount > 0 || hourCount > 0 || minCount > 0 || secCount >= 0) {
            stringBuilder.append(secCount).append("сек.");
        }

        return stringBuilder.toString();
    }

}
