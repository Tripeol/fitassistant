package com.tripeo.fitassistant.utils;

import androidx.annotation.NonNull;

import com.google.android.gms.fitness.data.DataType;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.MeditationHelper;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.PlankHelper;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.PullUpHelper;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.PushUpHelper;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.exercisecalculators.PressHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Trikhin P O
 * @since 11.05.2018
 *
 * Утилитный класс для {@link com.tripeo.fitassistant.mvp.model.ExerciseType} перечисляемого типа
 */

public class ExerciseTypeUtils {

    private static Map<ExerciseType, AbstractExersizeTypeHelper> sHelpersMap;
    private static List<AbstractExersizeTypeHelper> sHelpersList;

    static {
        sHelpersMap = new HashMap<>();
        sHelpersList = new ArrayList<>();

        PushUpHelper pushUpHelper = new PushUpHelper();
        sHelpersMap.put(ExerciseType.PUSH_UP, pushUpHelper);
        sHelpersList.add(pushUpHelper);

        MeditationHelper meditationHelper = new MeditationHelper();
        sHelpersMap.put(ExerciseType.MEDITATION, meditationHelper);
        sHelpersList.add(meditationHelper);

        PlankHelper plankHelper = new PlankHelper();
        sHelpersMap.put(ExerciseType.PLANK, plankHelper);
        sHelpersList.add(plankHelper);

        PullUpHelper pullUpHelper = new PullUpHelper();
        sHelpersMap.put(ExerciseType.PULL_UP, pullUpHelper);
        sHelpersList.add(pullUpHelper);

        PressHelper pressHelper = new PressHelper();
        sHelpersMap.put(ExerciseType.PRESS, pressHelper);
        sHelpersList.add(pressHelper);
    }

    /**
     * Получает {@link AbstractExersizeTypeHelper} через {@code ExerciseType}
     */
    @NonNull
    public static AbstractExersizeTypeHelper getHelperByType(ExerciseType type) {
        return sHelpersMap.get(type);
    }

    /**
     * Возвращает список всех {@link AbstractExersizeTypeHelper}
     */
    public static List<AbstractExersizeTypeHelper> getAllExerciseHelpers() {
        return sHelpersList;
    }

    /**
     * Возвращает сет всех {@link DataType}
     */
    public static Set<DataType> getDataTypes(List<ExerciseType> exerciseTypes) {
        Set<DataType> result = new HashSet<>();
        for (ExerciseType exerciseType : exerciseTypes) {
            result.add(getHelperByType(exerciseType).getDataType());
        }
        return result;
    }

}
