package com.tripeo.fitassistant.dagger;

import android.content.Context;

import com.tripeo.fitassistant.managers.implementations.DefaultFitnessRepository;
import com.tripeo.fitassistant.managers.implementations.DefaultPreferencesManager;
import com.tripeo.fitassistant.managers.implementations.DefaultResourceManager;
import com.tripeo.fitassistant.managers.implementations.DefaultTTSManager;
import com.tripeo.fitassistant.managers.implementations.DefaultUserSettingsManager;
import com.tripeo.fitassistant.managers.interfaces.IFitnessRepository;
import com.tripeo.fitassistant.managers.interfaces.IPreferenceManager;
import com.tripeo.fitassistant.managers.interfaces.IResourceManager;
import com.tripeo.fitassistant.managers.interfaces.ITTSManager;
import com.tripeo.fitassistant.managers.interfaces.IUserSettingsManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 *
 * Dagger Module для общеупотребимых менеджеров
 */

@Module
public class CommonManagersModule {

    @Provides
    @Singleton
    ITTSManager provideTTSManager(Context context) {
        return new DefaultTTSManager(context);
    }

    @Provides
    @Singleton
    IFitnessRepository provideFitnessManager(Context context, IUserSettingsManager userSettingsManager) {
        return new DefaultFitnessRepository(context, userSettingsManager);
    }

    @Provides
    @Singleton
    IPreferenceManager providePreferenceManager(Context context) {
        return new DefaultPreferencesManager(context);
    }

    @Provides
    @Singleton
    IUserSettingsManager provideUserSettingsManager(IPreferenceManager preferenceManager) {
        return new DefaultUserSettingsManager(preferenceManager);
    }

    @Provides
    @Singleton
    IResourceManager provideResourceManager(Context context) {
        return new DefaultResourceManager(context);
    }
}
