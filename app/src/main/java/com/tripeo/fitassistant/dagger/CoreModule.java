package com.tripeo.fitassistant.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 */

@Module
public class CoreModule {

    private Context mContext;

    public CoreModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }

}
