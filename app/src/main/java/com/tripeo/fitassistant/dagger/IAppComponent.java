package com.tripeo.fitassistant.dagger;

import com.tripeo.fitassistant.mvp.view.fragments.SettingsFragment;
import com.tripeo.fitassistant.mvp.viewmodel.DataPointActionsDialogViewModel;
import com.tripeo.fitassistant.mvp.viewmodel.ExerciseActivityViewModel;
import com.tripeo.fitassistant.mvp.viewmodel.MainFragmentViewModel;
import com.tripeo.fitassistant.mvp.viewmodel.SettingsFragmentViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 *
 * Dagger компонент распространяющийся на все приложение
 */

@Component(modules = {CoreModule.class, AIModule.class, CommonManagersModule.class, CommonPresentersModule.class})
@Singleton
public interface IAppComponent {

    void inject(SettingsFragment settingsFragment);

    void inject(SettingsFragmentViewModel settingsFragmentViewModel);

    void inject(MainFragmentViewModel mainFragmentViewModel);

    void inject(ExerciseActivityViewModel exerciseActivityViewModel);

    void inject(DataPointActionsDialogViewModel viewModel);

}
