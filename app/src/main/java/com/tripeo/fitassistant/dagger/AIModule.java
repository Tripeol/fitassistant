package com.tripeo.fitassistant.dagger;

import android.content.Context;

import com.tripeo.fitassistant.managers.implementations.DefaultAIManager;
import com.tripeo.fitassistant.managers.implementations.DefaultAIParser;
import com.tripeo.fitassistant.managers.interfaces.IAIManager;
import com.tripeo.fitassistant.managers.interfaces.IAIParser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 */

@Module
public class AIModule {

    @Provides
    @Singleton
    IAIManager provideAiManager(Context context) {
        return new DefaultAIManager(context);
    }

    @Provides
    @Singleton
    IAIParser provideAiParser() {
        return new DefaultAIParser();
    }
}
