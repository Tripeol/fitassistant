package com.tripeo.fitassistant.managers.implementations;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;

import com.tripeo.fitassistant.managers.interfaces.IPreferenceManager;

/**
 * Дефолтная реализация {@link com.tripeo.fitassistant.managers.interfaces.IPreferenceManager}
 */
public class DefaultPreferencesManager implements IPreferenceManager {

    public static final String SHARED_PREFERENCES_NAME = "sharedPrefs";

    private SharedPreferences mSharedPreferences;

    public DefaultPreferencesManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putBoolean(String key, boolean vlue) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, vlue);
        editor.apply();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unregisterPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }
}
