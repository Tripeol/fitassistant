package com.tripeo.fitassistant.managers.interfaces;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;

import java.util.Date;
import java.util.List;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 * <p>
 * Менеджер для работы с GoogleFit
 */

public interface IFitnessRepository {

    /**
     * Метод для чтения данных из GoogleFit
     *
     * @param exerciseTypes     список типов упражнений которые мы хотим получить
     * @param from              дата начиная с которой получать данные
     * @param to                дата до которой получать данные
     * @param onSuccessListener обработчик успешного запроса
     * @param onFailureListener обработчик неуспешного запроса
     */
    void readData(@NonNull List<ExerciseType> exerciseTypes,
                  @NonNull Date from,
                  @NonNull Date to,
                  @Nullable OnSuccessListener<DataReadResponse> onSuccessListener,
                  @Nullable OnFailureListener onFailureListener);

    /**
     * Тестовый метод для удаления данных
     *
     * @param dataPoint подход который будет удален
     * @param onSuccessListener слушатель успешного удаления
     * @param onFailureListener слушатель ошибки удаления
     */
    void deleteData(@NonNull DataPoint dataPoint,
                    @Nullable OnSuccessListener<Void> onSuccessListener,
                    @Nullable OnFailureListener onFailureListener);

    /**
     * Метод для записи данных
     */
    void writeData(ExerciseSet exerciseSet,
                   @Nullable OnSuccessListener<Void> onSuccessListener,
                   @Nullable OnFailureListener onFailureListener);
}
