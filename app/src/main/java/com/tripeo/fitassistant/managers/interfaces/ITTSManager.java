package com.tripeo.fitassistant.managers.interfaces;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 *
 * Менеджер для перевода текста в речь
 */

public interface ITTSManager {

    /**
     * Проговорить голосом ассистента указанный текст
     * @param text текст который требуется проговорить голосом ассистента
     */
    public void toSpeech(String text);
}
