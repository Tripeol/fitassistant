package com.tripeo.fitassistant.managers.implementations;

import android.text.format.DateUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Trikhin P O
 * @since 05.05.2018
 */

public enum DurationUnit {

    SECOND("s", DateUtils.SECOND_IN_MILLIS),
    MIN("min", DateUtils.MINUTE_IN_MILLIS),
    HOUR("hour", DateUtils.HOUR_IN_MILLIS),
    DAY("day", DateUtils.DAY_IN_MILLIS);

    private static Map<String, DurationUnit> sMap;

    static {
        sMap = new HashMap<>();
        for (DurationUnit durationUnit : DurationUnit.values()) {
            sMap.put(durationUnit.mKey, durationUnit);
        }
    }

    private String mKey;

    private long mTimeMultiplier;

    DurationUnit(String key, long timeMultiplier) {
        mKey = key;
        mTimeMultiplier = timeMultiplier;
    }

    public long getTimeMultiplier() {
        return mTimeMultiplier;
    }

    public static DurationUnit getFromString(String key) {
        return sMap.get(key);
    }
}
