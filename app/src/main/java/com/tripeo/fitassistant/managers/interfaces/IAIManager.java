package com.tripeo.fitassistant.managers.interfaces;

import androidx.annotation.Nullable;

import ai.api.AIListener;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 *
 * Менеджер отвечающий за голосовое управление
 */

public interface IAIManager {

    /**
     * Проставить слушателя на результат распознования голоса, может быть null
     */
    void setListener (@Nullable AIListener aiListener);

    /**
     * Начать прослушивание пользователя
     */
    void startListening();

    /**
     * Закончить прослушивание пользователя
     */
    void stopListening();

}
