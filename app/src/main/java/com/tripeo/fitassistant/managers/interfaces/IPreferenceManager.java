package com.tripeo.fitassistant.managers.interfaces;

import android.content.SharedPreferences;
import android.preference.Preference;

/**
 * Менеджер для работы с {@link android.content.SharedPreferences}
 */
public interface IPreferenceManager {

    /**
     * Записать boolean значение в настройки
     *
     * @param key   ключ
     * @param value значение
     */
    void putBoolean(String key, boolean value);

    /**
     * Прочитать boolean значение из настроек
     *
     * @param key          ключ
     * @param defaultValue значение по умолчанию
     */
    boolean getBoolean(String key, boolean defaultValue);

    /**
     * Подписать слушателя на изменения настроек
     */
    void registerPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener);

    /**
     * Отписать слушателя на изменения настроек
     */
    void unregisterPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener);

}