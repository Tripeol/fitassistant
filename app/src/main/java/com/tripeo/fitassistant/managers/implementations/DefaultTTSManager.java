package com.tripeo.fitassistant.managers.implementations;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import com.tripeo.fitassistant.managers.interfaces.ITTSManager;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 *
 * Дефолтная реализация {@link com.tripeo.fitassistant.managers.interfaces.ITTSManager}
 */

public class DefaultTTSManager implements ITTSManager {

    private TextToSpeech mTextToSpeech;

    public DefaultTTSManager(Context context) {
        if (mTextToSpeech == null) {
            mTextToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int i) {

                }
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void toSpeech(String text) {
        mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, String.valueOf(System.currentTimeMillis()));
    }
}
