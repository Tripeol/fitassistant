package com.tripeo.fitassistant.managers.implementations;

import androidx.annotation.Nullable;

import com.google.gson.JsonElement;
import com.tripeo.fitassistant.managers.interfaces.IAIParser;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;
import com.tripeo.fitassistant.utils.ExerciseTypeUtils;

import java.util.Map;

/**
 * @author Trikhin P O
 * @since 01.05.2018
 *
 * Дефолтная реализация {@link IAIParser}
 */

public class DefaultAIParser implements IAIParser {

    private static final String TYPE_KEY = "type";
    private static final String NUMBER_KEY = "number";
    private static final String DURATION_KEY = "duration";

    private static final String TIMED_EXERSIZE_TYPE_KEY = "timed_exercise_type";
    private static final String DURATION_AMOUNT_KEY = "amount";
    private static final String DURATION_UNIT_KEY = "unit";

    public DefaultAIParser() {
    }

    @Override
    public ExerciseSet parse(Map<String, JsonElement> map) {

        ExerciseSet exerciseSet = new ExerciseSet();
        if (map.containsKey(TYPE_KEY)) {
            JsonElement jsonElement = map.get(TYPE_KEY);
            if (jsonElement != null) {
                exerciseSet.setType(parseTypeFromString(jsonElement.getAsString()));
            }
        }
        if (map.containsKey(NUMBER_KEY)) {
            JsonElement jsonElement = map.get(NUMBER_KEY);
            if (jsonElement != null) {
                exerciseSet.setRepetitions(jsonElement.getAsInt());
            }
        }

        if (map.containsKey(TIMED_EXERSIZE_TYPE_KEY)) {
            JsonElement jsonElement = map.get(TIMED_EXERSIZE_TYPE_KEY);
            if (jsonElement != null) {
                exerciseSet.setType(parseTypeFromString(jsonElement.getAsString()));
            }
        }
        if (map.containsKey(DURATION_KEY)) {
            int durationAmount = map.get(DURATION_KEY).getAsJsonObject().get(DURATION_AMOUNT_KEY).getAsInt();
            DurationUnit durationUnit = DurationUnit.getFromString(map.get(DURATION_KEY).getAsJsonObject().get(DURATION_UNIT_KEY).getAsString());
            exerciseSet.setDuration(durationAmount * durationUnit.getTimeMultiplier());
        }


        //map.get("duration").getAsJsonObject().get("amount")

        return exerciseSet;
    }

    @Nullable
    private ExerciseType parseTypeFromString(String value) {
        for (AbstractExersizeTypeHelper abstractExersizeTypeHelper : ExerciseTypeUtils.getAllExerciseHelpers()) {
            if (abstractExersizeTypeHelper.parseDialogFlowKey(value)) {
                return abstractExersizeTypeHelper.getType();
            }
        }
        return null;
    }
}
