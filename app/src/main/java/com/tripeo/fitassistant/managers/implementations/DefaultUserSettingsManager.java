package com.tripeo.fitassistant.managers.implementations;

import android.content.SharedPreferences;

import com.tripeo.fitassistant.managers.interfaces.IPreferenceManager;
import com.tripeo.fitassistant.managers.interfaces.IUserSettingsManager;

/**
 * Дефолтная реализация {@link com.tripeo.fitassistant.managers.interfaces.IUserSettingsManager}
 *
 * @author Trikhin P O
 */
public class DefaultUserSettingsManager implements IUserSettingsManager {

    public DefaultUserSettingsManager(IPreferenceManager preferenceManager) {
        mPreferenceManager = preferenceManager;
    }

    IPreferenceManager mPreferenceManager;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDeveloperModeOn() {
        return mPreferenceManager.getBoolean(DEBUG_MODE_KEY, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mPreferenceManager.registerPreferenceListener(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unregisterPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mPreferenceManager.unregisterPreferenceListener(listener);
    }
}
