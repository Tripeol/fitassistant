package com.tripeo.fitassistant.managers.implementations;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataDeleteRequest;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataUpdateRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.tripeo.fitassistant.managers.interfaces.IFitnessRepository;
import com.tripeo.fitassistant.managers.interfaces.IUserSettingsManager;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;
import com.tripeo.fitassistant.mvp.model.ExerciseType;
import com.tripeo.fitassistant.parsers.exersizetypeparsers.AbstractExersizeTypeHelper;
import com.tripeo.fitassistant.utils.ExerciseTypeUtils;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 * <p>
 * Дефолтная реализация для {@link IFitnessRepository}
 */

public class DefaultFitnessRepository implements IFitnessRepository {

    private static final String LOG_TAG = "FitnessRepository";

    private Context mContext;
    private IUserSettingsManager mUserSettingsManager;

    public DefaultFitnessRepository(@NonNull Context context, @NonNull IUserSettingsManager userSettingsManager) {
        mContext = context;
        mUserSettingsManager = userSettingsManager;

        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(mContext);
        if (googleSignInAccount != null) {
            Fitness.getRecordingClient(mContext, googleSignInAccount)
                    .subscribe(DataType.TYPE_WORKOUT_EXERCISE)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                        }
                    });
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void readData(@NonNull List<ExerciseType> exerciseTypes,
                         @NonNull Date from,
                         @NonNull Date to,
                         @Nullable OnSuccessListener<DataReadResponse> onSuccessListener,
                         @Nullable OnFailureListener onFailureListener) {

        DataReadRequest.Builder builder = new DataReadRequest.Builder();
        for (DataType dataType : ExerciseTypeUtils.getDataTypes(exerciseTypes)) {
            builder.read(dataType);
        }
        builder.setTimeRange(from.getTime(), to.getTime(), TimeUnit.MILLISECONDS);
        builder.enableServerQueries();

        DataReadRequest readRequest = builder.build();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mContext);
        if (account != null) {
            Task<DataReadResponse> task = Fitness.getHistoryClient(mContext, account)
                    .readData(readRequest);
            if (onSuccessListener != null) {
                task.addOnSuccessListener(onSuccessListener);
            }
            if (onFailureListener != null) {
                task.addOnFailureListener(onFailureListener);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteData(@NonNull DataPoint dataPoint,
                           @Nullable OnSuccessListener<Void> onSuccessListener,
                           @Nullable OnFailureListener onFailureListener) {
        DataDeleteRequest request =
                new DataDeleteRequest.Builder()
                        .setTimeInterval(
                                dataPoint.getStartTime(TimeUnit.MILLISECONDS),
                                dataPoint.getEndTime(TimeUnit.MILLISECONDS),
                                TimeUnit.MILLISECONDS)
                        .addDataType(dataPoint.getDataType())
                        .build();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mContext);
        if (account != null) {
            Task<Void> task = Fitness.getHistoryClient(mContext, account)
                    .deleteData(request);
            if (onSuccessListener != null) {
                task.addOnSuccessListener(onSuccessListener);
            }
            if (onFailureListener != null) {
                task.addOnFailureListener(onFailureListener);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeData(ExerciseSet exerciseSet,
                          @Nullable OnSuccessListener<Void> onSuccessListener,
                          @Nullable OnFailureListener onFailureListener) {

        AbstractExersizeTypeHelper abstractExersizeTypeHelper = ExerciseTypeUtils.getHelperByType(exerciseSet.getType());

        DataSource dataSource =
                new DataSource.Builder()
                        .setAppPackageName(mContext)
                        .setDataType(abstractExersizeTypeHelper.getDataType())
                        .setType(DataSource.TYPE_RAW)
                        .build();

        DataSet dataSet = DataSet.create(dataSource);

        DataPoint dataPoint = abstractExersizeTypeHelper.constructDataPoint(dataSet, exerciseSet);

        dataSet.add(dataPoint);

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mContext);
        if (account != null) {
            DataUpdateRequest request = new DataUpdateRequest.Builder()
                    .setDataSet(dataSet)
                    .setTimeInterval(dataPoint.getStartTime(TimeUnit.MILLISECONDS), dataPoint.getEndTime(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS)
                    .build();
            Task task = Fitness.getHistoryClient(mContext, account).updateData(request);

            if (onSuccessListener != null) {
                task.addOnSuccessListener(onSuccessListener);
            }
            if (onFailureListener != null) {
                task.addOnFailureListener(onFailureListener);
            }
        }
    }
}
