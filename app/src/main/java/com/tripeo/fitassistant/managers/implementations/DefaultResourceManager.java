package com.tripeo.fitassistant.managers.implementations;

import android.content.Context;

import com.tripeo.fitassistant.managers.interfaces.IResourceManager;

/**
 * Дефолтная реализация {@link com.tripeo.fitassistant.managers.interfaces.IResourceManager}
 *
 * @author Trikhin P O on 17.10.2018
 */
public class DefaultResourceManager implements IResourceManager {

    private Context mContext;

    public DefaultResourceManager(Context context) {
        mContext = context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStringFromResource(int stringRes) {
        return mContext.getString(stringRes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getColorFromResource(int colorRes) {
        return mContext.getResources().getColor(colorRes);
    }
}
