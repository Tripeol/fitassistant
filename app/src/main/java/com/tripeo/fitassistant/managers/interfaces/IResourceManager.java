package com.tripeo.fitassistant.managers.interfaces;

import androidx.annotation.ColorRes;
import androidx.annotation.StringRes;

/**
 * Менеджер для доступа к ресурсам приложения
 * В основном нужен для использования внутри классов типа Presenter или ViewModel в которых нет контекста,
 * но в которых необходимо получить ресурсы приложения для обработки
 *
 * @author Trikhin P O on 17.10.2018
 */
public interface IResourceManager {

    /**
     * Получение строки из идентификатора ресурса
     */
    String getStringFromResource(@StringRes int stringRes);

    /**
     * Получение цвета из идентификатора ресурса
     */
    int getColorFromResource(@ColorRes int colorRes);

}
