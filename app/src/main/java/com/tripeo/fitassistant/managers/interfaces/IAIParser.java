package com.tripeo.fitassistant.managers.interfaces;

import com.google.gson.JsonElement;
import com.tripeo.fitassistant.mvp.model.ExerciseSet;

import java.util.Map;

/**
 * @author Trikhin P O
 * @since 01.05.2018
 *
 * Парсер ответа голосового ассистента
 */

public interface IAIParser {

    public ExerciseSet parse (Map<String, JsonElement> map);

}
