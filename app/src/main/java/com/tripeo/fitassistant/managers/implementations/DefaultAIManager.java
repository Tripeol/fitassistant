package com.tripeo.fitassistant.managers.implementations;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import androidx.annotation.Nullable;

import com.tripeo.fitassistant.managers.interfaces.IAIManager;

import ai.api.AIListener;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;

/**
 * @author Trikhin P O
 * @date 28.04.2018
 *
 * Дефолтная реализация {@link IAIManager}
 */

public class DefaultAIManager implements IAIManager {

    private static final String AI_SERVICE_CLIENT_TOKEN = "f2871271f74d4cbfb5d9e43c110abf54";

    private AIConfiguration mConfig;
    private AIService mAiService;

    private TextToSpeech mTextToSpeech;

    public DefaultAIManager(Context context) {
        mConfig = new AIConfiguration(AI_SERVICE_CLIENT_TOKEN,
                AIConfiguration.SupportedLanguages.Russian,
                AIConfiguration.RecognitionEngine.System);

        mAiService = AIService.getService(context, mConfig);

        if (mTextToSpeech == null) {
            mTextToSpeech = new TextToSpeech(context, i -> {});
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setListener(@Nullable AIListener aiListener) {
        mAiService.setListener(aiListener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startListening() {
        mAiService.startListening();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopListening() {
        mAiService.stopListening();
    }
}
