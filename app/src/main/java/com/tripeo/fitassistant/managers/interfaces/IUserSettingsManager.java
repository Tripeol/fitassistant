package com.tripeo.fitassistant.managers.interfaces;

import android.content.SharedPreferences;

/**
 * Менеджер отвечающий за настройки пользователя с экрана настроек
 *
 * @author Trikhin P O
 */
public interface IUserSettingsManager {

    String DEBUG_MODE_KEY = "debugModeKey";

    /**
     * Включен ли режим разработчика
     */
    boolean isDeveloperModeOn ();

    /**
     * Подписать слушателя на изменения настроек
     */
    void registerPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener);

    /**
     * Отписать слушателя на изменения настроек
     */
    void unregisterPreferenceListener(SharedPreferences.OnSharedPreferenceChangeListener listener);

}
